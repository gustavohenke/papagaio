<?php
/**
 * Arquivo de configuração para operações via CLI do Doctrine.
 * Favor não alterar.
 */

// Carrega o autoloader do Composer
require_once 'vendor/autoload.php';

use Doctrine\ORM\Tools\Console\ConsoleRunner;

// Instancia nosso DatabaseManager e obtem o EntityManager do Doctrine
$dbManager = \Papagaio\Core\DatabaseManager::getInstance();
$em = $dbManager->getEntityManager();

return ConsoleRunner::createHelperSet( $em );