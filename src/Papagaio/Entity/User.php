<?php

namespace Papagaio\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Papagaio\Core\App;
use Papagaio\Entity\Visitor\EntityVisitor;

/**
 * Entidade User
 *
 * @Entity
 * @Table(name="users")
 * @package Papagaio\Entity
 */
class User implements Entity {

    /**
     * @var int
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /**
     * @var string
     * @Column(unique=true)
     */
    private $username;

    /**
     * @var string
     * @Column()
     */
    private $password;

    /**
     * @var string
     * @Column(unique=true)
     */
    private $email;

    /**
     * @var UserPhoto
     * @OneToOne(targetEntity="UserPhoto", mappedBy="user", cascade={"ALL"})
     */
    private $photo;

    /**
     * @var UserCover
     */
    private $cover;

    /**
     * @var \DateTime
     * @Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var ArrayCollection
     * @OneToMany(targetEntity="Follow", mappedBy="follower")
     */
    private $following;

    /**
     * @var ArrayCollection
     * @OneToMany(targetEntity="Follow", mappedBy="user")
     */
    private $followers;

    public function __construct () {
        $this->followers = new ArrayCollection();
        $this->following = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId () {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId ( $id ) {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUsername () {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername ( $username ) {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword () {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword ( $password ) {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getEmail () {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail ( $email ) {
        $this->email = $email;
    }

    /**
     * @return  string
     */
    public function getUrl () {
        $app = App::getInstance();
        return $app->base() . $app->buildUrl( 'user', [
            'user' => $this->getUsername()
        ]);
    }

    /**
     * @return UserPhoto
     */
    public function getPhoto () {
        return $this->photo;
    }

    /**
     * @param UserPhoto $photo
     */
    public function setPhoto ( UserPhoto $photo ) {
        $this->photo = $photo;
        if ( $photo != null ) {
            $photo->setUser( $this );
            $this->cover = null;
        }
    }

    /**
     * @return UserCover
     */
    public function getCover () {
        if ( $this->cover == null ) {
            $this->cover = new UserCover();
            $this->cover->setUser( $this );
            $this->cover->setContent( $this->photo->getContent() );
            $this->cover->setType( $this->photo->getType() );
            $this->cover->setUpdatedAt( $this->photo->getUpdatedAt() );
        }

        return $this->cover;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt () {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt ( $createdAt ) {
        $this->createdAt = $createdAt;
    }

    /**
     * @return ArrayCollection
     */
    public function getFollowers () {
        return $this->followers;
    }

    /**
     * @param ArrayCollection $followers
     */
    public function setFollowers ( $followers ) {
        $this->followers = $followers;
    }

    /**
     * @return ArrayCollection
     */
    public function getFollowing () {
        return $this->following;
    }

    /**
     * @param ArrayCollection $following
     */
    public function setFollowing ( $following ) {
        $this->following = $following;
    }

    /**
     * @param   EntityVisitor $visitor
     * @return  mixed
     */
    public function accept ( EntityVisitor $visitor ) {
        return $visitor->visitUser( $this );
    }

}