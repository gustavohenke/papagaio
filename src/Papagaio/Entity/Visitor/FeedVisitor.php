<?php

namespace Papagaio\Entity\Visitor;

use Papagaio\Entity\Follow;
use Papagaio\Entity\HashTag;
use Papagaio\Entity\Like;
use Papagaio\Entity\Mention;
use Papagaio\Entity\Notification;
use Papagaio\Entity\Post;
use Papagaio\Entity\PostPhoto;
use Papagaio\Entity\Session;
use Papagaio\Entity\User;
use Papagaio\Entity\UserImage;
use Suin\RSSWriter\Item;

class FeedVisitor implements EntityVisitor {

    /**
     * Visita um User
     *
     * @param   User $user
     * @return  mixed
     */
    public function visitUser ( User $user ) {
        // TODO: Implement visitUser() method.
    }

    /**
     * Visita um Post
     * @param   Post $post
     * @return  mixed
     */
    public function visitPost ( Post $post ) {
        $item = new Item();
        $item->url( $post->getHtmlUrl() );
        $item->guid( $post->getHtmlUrl() );
        $item->title( $post->getText() );
        $item->description( $post->getText() );
        $item->pubDate( $post->getCreatedAt()->getTimestamp() );
        return $item;
    }

    /**
     * Visita um Session
     *
     * @param   Session $session
     * @return  mixed
     */
    public function visitSession ( Session $session ) {
        // TODO: Implement visitSession() method.
    }

    /**
     * Visita um UserPhoto
     *
     * @param   UserImage $userImage
     * @return  mixed
     */
    public function visitUserImage ( UserImage $userImage ) {
        // TODO: Implement visitUserImage() method.
    }

    /**
     * Visita um PostPhoto
     *
     * @param   PostPhoto $postPhoto
     * @return  mixed
     */
    public function visitPostPhoto ( PostPhoto $postPhoto ) {
        // TODO: Implement visitPostPhoto() method.
    }

    /**
     * Visita uma HashTag
     *
     * @param   HashTag $hashTag
     * @return  mixed
     */
    public function visitHashTag ( HashTag $hashTag ) {
        // TODO: Implement visitHashTag() method.
    }

    /**
     * Visita uma Mention
     *
     * @param   Mention $mention
     * @return  mixed
     */
    public function visitMention ( Mention $mention ) {
        // TODO: Implement visitMention() method.
    }

    /**
     * Visita uma Notification
     *
     * @param   Notification $notification
     * @return  mixed
     */
    public function visitNotification ( Notification $notification ) {
        // TODO: Implement visitNotification() method.
    }

    /**
     * Visita um Like
     *
     * @param   Like $like
     * @return  mixed
     */
    public function visitLike ( Like $like ) {
        // TODO: Implement visitLike() method.
    }

    /**
     * Visita um Follow
     *
     * @param   Follow $follow
     * @return  mixed
     */
    public function visitFollow ( Follow $follow ) {
        // TODO: Implement visitFollow() method.
    }
}