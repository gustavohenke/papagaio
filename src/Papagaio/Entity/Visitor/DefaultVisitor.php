<?php

namespace Papagaio\Entity\Visitor;

use Papagaio\Core\App;
use Papagaio\Entity\Follow;
use Papagaio\Entity\HashTag;
use Papagaio\Entity\Like;
use Papagaio\Entity\Mention;
use Papagaio\Entity\Notification;
use Papagaio\Entity\Post;
use Papagaio\Entity\PostPhoto;
use Papagaio\Entity\Session;
use Papagaio\Entity\User;
use Papagaio\Entity\UserImage;
use Papagaio\Entity\UserPhoto;
use Papagaio\Model\NotificationModel;

class DefaultVisitor implements EntityVisitor {

    /**
     * Visita um User
     *
     * @param   User $user
     * @return  mixed
     */
    public function visitUser ( User $user ) {
        return [
            'id'        => $user->getId(),
            'username'  => $user->getUsername(),
            'createdAt' => $user->getCreatedAt(),
            'url'       => $user->getUrl(),
            'htmlUrl'   => preg_replace( '#/api#', '', $user->getUrl(), 1 ),
            'photo'     => $user->getPhoto(),
            'cover'     => $user->getCover(),
            'followersCount' => $user->getFollowers()->count(),
            'followingCount' => $user->getFollowing()->count(),
        ];
    }

    /**
     * Visita um Post
     * @param   Post $post
     * @return  mixed
     */
    public function visitPost ( Post $post ) {
        $mentions = $post->getMentions()->toArray();

        /** @var Mention $mention */
        foreach ( $mentions as &$mention ) {
            $mention = $mention->getUser();
        }

        return [
            'id'        => $post->getId(),
            'text'      => $post->getText(),
            'author'    => $post->getAuthor(),
            'location'  => empty( $post->getLocation() ) ? null : $post->getLocation(),
            'createdAt' => $post->getCreatedAt(),
            'photos'    => $post->getPhotos(),
            'hashtags'  => $post->getHashTags(),
            'mentions'  => $mentions,
            'url'       => $post->getUrl(),
            'htmlUrl'   => $post->getHtmlUrl(),
            'likesCount' => $post->getLikes()->count()
        ];
    }

    /**
     * Visita um Session
     *
     * @param   Session $session
     * @return  mixed
     */
    public function visitSession ( Session $session ) {
        return [
            'id'        => $session->getId(),
            'user'      => $session->getUser(),
            'createdAt' => $session->getCreatedAt()
        ];
    }

    /**
     * Visita um UserImage
     *
     * @param   UserImage $userImage
     * @return  mixed
     */
    public function visitUserImage ( UserImage $userImage ) {
        return [
            'url'   => $userImage->getUrl()
        ];
    }

    /**
     * Visita um PostPhoto
     *
     * @param   PostPhoto $postPhoto
     * @return  mixed
     */
    public function visitPostPhoto ( PostPhoto $postPhoto ) {
        return [
            'url'   => $postPhoto->getUrl()
        ];
    }

    /**
     * Visita uma HashTag
     *
     * @param   HashTag $hashTag
     * @return  mixed
     */
    public function visitHashTag ( HashTag $hashTag ) {
        return [
            'id'        => $hashTag->getId(),
            'url'       => $hashTag->getUrl(),
            'postsUrl'  => $hashTag->getPostsUrl(),
        ];
    }

    /**
     * Visita uma Mention
     *
     * @param   Mention $mention
     * @return  mixed
     */
    public function visitMention ( Mention $mention ) {
        $post = $mention->getPost()->accept( $this );
        unset( $post[ 'mentions' ] );

        return [
            'id'    => $mention->getId(),
            'user'  => $mention->getUser(),
            'post'  => $post
        ];
    }

    /**
     * Visita uma Notification
     *
     * @param   Notification $notification
     * @return  mixed
     */
    public function visitNotification ( Notification $notification ) {
        /** @var NotificationModel $notificationModel */
        $notificationModel = App::getInstance()->model( 'notification' );
        $relatedObject = $notificationModel->findRelatedObject( $notification );
        $sender = $notificationModel->findSender( $notification );

        return [
            'id'        => $notification->getId(),
            'type'      => $notification->getType(),
            'relation'  => $notification->getRelationId(),
            'object'    => $relatedObject,
            'sender'    => $sender,
            'unread'    => $notification->isUnread(),
            'createdAt' => $notification->getCreatedAt()
        ];
    }

    /**
     * Visita um Like
     *
     * @param   Like $like
     * @return  mixed
     */
    public function visitLike ( Like $like ) {
        return [
            'id'        => $like->getId(),
            'post'      => $like->getPost(),
            'user'      => $like->getUser(),
            'createdAt' => $like->getCreatedAt()
        ];
    }

    /**
     * Visita um Follow
     *
     * @param   Follow $follow
     * @return  mixed
     */
    public function visitFollow ( Follow $follow ) {
        return [
            'user'      => $follow->getUser(),
            'follower'  => $follow->getFollower(),
            'createdAt' => $follow->getCreatedAt()
        ];
    }


}