<?php

namespace Papagaio\Entity\Visitor;

use Papagaio\Entity\Follow;
use Papagaio\Entity\HashTag;
use Papagaio\Entity\Like;
use Papagaio\Entity\Mention;
use Papagaio\Entity\Notification;
use Papagaio\Entity\Post;
use Papagaio\Entity\PostPhoto;
use Papagaio\Entity\Session;
use Papagaio\Entity\User;
use Papagaio\Entity\UserImage;
use Papagaio\Entity\UserPhoto;

interface EntityVisitor {

    /**
     * Visita um User
     *
     * @param   User $user
     * @return  mixed
     */
    public function visitUser ( User $user );

    /**
     * Visita um Post
     * @param   Post $post
     * @return  mixed
     */
    public function visitPost ( Post $post );

    /**
     * Visita um Session
     *
     * @param   Session $session
     * @return  mixed
     */
    public function visitSession ( Session $session );

    /**
     * Visita um UserPhoto
     *
     * @param   UserImage $userImage
     * @return  mixed
     */
    public function visitUserImage ( UserImage $userImage );

    /**
     * Visita um PostPhoto
     *
     * @param   PostPhoto $postPhoto
     * @return  mixed
     */
    public function visitPostPhoto ( PostPhoto $postPhoto );

    /**
     * Visita uma HashTag
     *
     * @param   HashTag $hashTag
     * @return  mixed
     */
    public function visitHashTag ( HashTag $hashTag );

    /**
     * Visita uma Mention
     *
     * @param   Mention $mention
     * @return  mixed
     */
    public function visitMention ( Mention $mention );

    /**
     * Visita uma Notification
     *
     * @param   Notification $notification
     * @return  mixed
     */
    public function visitNotification ( Notification $notification );

    /**
     * Visita um Like
     *
     * @param   Like $like
     * @return  mixed
     */
    public function visitLike ( Like $like );

    /**
     * Visita um Follow
     *
     * @param   Follow $follow
     * @return  mixed
     */
    public function visitFollow ( Follow $follow );

}