<?php

namespace Papagaio\Entity\Event;


use Doctrine\ORM\Event\LifecycleEventArgs;

interface PreRemoveEvent {

    /**
     * @PreRemove
     * @param   object $instance
     * @param   LifecycleEventArgs $args
     * @return  void
     */
    public function onPreRemove ( $instance, LifecycleEventArgs $args );

}