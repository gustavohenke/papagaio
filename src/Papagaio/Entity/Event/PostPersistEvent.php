<?php

namespace Papagaio\Entity\Event;

use Doctrine\ORM\Event\LifecycleEventArgs;

interface PostPersistEvent {

    /**
     * @PostPersist
     * @param   object $instance
     * @param   LifecycleEventArgs $args
     * @return  void
     */
    public function onPostPersist ( $instance, LifecycleEventArgs $args );

}