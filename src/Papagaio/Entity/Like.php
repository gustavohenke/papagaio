<?php

namespace Papagaio\Entity;

use Papagaio\Entity\Visitor\EntityVisitor;

/**
 * Entidade Like
 *
 * @Entity
 * @Table(name="likes")
 * @EntityListeners({
 *      "Papagaio\Model\Event\SaveLikeNotificationEvent",
 *      "Papagaio\Model\Event\RemoveLikeNotificationEvent"
 * })
 * @package Papagaio\Entity
 */
class Like implements Entity {

    /**
     * @Id
     * @Column(type="guid")
     * @GeneratedValue(strategy="UUID")
     * @var string
     */
    private $id;

    /**
     * @var User
     * @ManyToOne(targetEntity="User")
     */
    private $user;

    /**
     * @var Post
     * @ManyToOne(targetEntity="Post")
     */
    private $post;

    /**
     * @var \DateTime
     * @Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * @return string
     */
    public function getId () {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId ( $id ) {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getUser () {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser ( $user ) {
        $this->user = $user;
    }

    /**
     * @return Post
     */
    public function getPost () {
        return $this->post;
    }

    /**
     * @param Post $post
     */
    public function setPost ( $post ) {
        $this->post = $post;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt () {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt ( $createdAt ) {
        $this->createdAt = $createdAt;
    }

    /**
     * Aceita a visita na entidade.
     *
     * @param   EntityVisitor $visitor
     * @return  mixed
     */
    public function accept ( EntityVisitor $visitor ) {
        return $visitor->visitLike( $this );
    }
}