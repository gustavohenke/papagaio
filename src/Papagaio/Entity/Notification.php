<?php

namespace Papagaio\Entity;

use Papagaio\Entity\Visitor\EntityVisitor;

/**
 * Entidade Notification
 *
 * @Entity
 * @Table(name="notifications")
 * @package Papagaio\Entity
 */
class Notification implements Entity {

    const TYPE_MENTION = 'MENTION';
    const TYPE_LIKE    = 'LIKE';
    const TYPE_FOLLOW  = 'FOLLOW';

    public static $allowed_types = [
        self::TYPE_LIKE,
        self::TYPE_MENTION,
        self::TYPE_FOLLOW
    ];

    /**
     * @Id
     * @Column(type="bigint")
     * @GeneratedValue
     * @var string
     */
    private $id;

    /**
     * @var string
     * @Column
     */
    private $type;

    /**
     * @var string
     * @Column
     */
    private $relationId;

    /**
     * @var boolean
     * @Column(type="boolean")
     */
    private $unread;

    /**
     * @var User
     * @ManyToOne(targetEntity="User")
     */
    private $user;

    /**
     * @var \DateTime
     * @Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @return string
     */
    public function getId () {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId ( $id ) {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getType () {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType ( $type ) {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getRelationId () {
        return $this->relationId;
    }

    /**
     * @param string $relationId
     */
    public function setRelationId ( $relationId ) {
        $this->relationId = $relationId;
    }

    /**
     * @return boolean
     */
    public function isUnread () {
        return $this->unread;
    }

    /**
     * @param boolean $unread
     */
    public function setUnread ( $unread ) {
        $this->unread = $unread;
    }

    /**
     * @return User
     */
    public function getUser () {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser ( $user ) {
        $this->user = $user;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt () {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt ( $createdAt ) {
        $this->createdAt = $createdAt;
    }

    /**
     * Aceita a visita na entidade.
     *
     * @param   EntityVisitor $visitor
     * @return  mixed
     */
    public function accept ( EntityVisitor $visitor ) {
        return $visitor->visitNotification( $this );
    }
}