<?php

namespace Papagaio\Entity;

use Papagaio\Core\App;

/**
 * Entidade UserPhoto
 *
 * @Entity
 * @Table("user_photos")
 */
class UserPhoto extends UserImage {

    /**
     * @return string
     */
    public function getUrl () {
        $app = App::getInstance();
        return $app->base() . $app->buildUrl( 'userphoto', [
            'user' => $this->getUser()->getUsername()
        ]);
    }

}