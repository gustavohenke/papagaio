<?php

namespace Papagaio\Entity;

use Papagaio\Entity\Visitor\EntityVisitor;

/**
 * Entidade UserPhoto
 *
 * @MappedSuperClass
 * @package Papagaio\Entity
 */
abstract class UserImage implements Entity {

    /**
     * @var User
     * @Id
     * @OneToOne(targetEntity="User", inversedBy="photo")
     */
    private $user;

    /**
     * @Column(type="blob", nullable=true)
     */
    private $content;

    /**
     * @var \DateTime
     * @Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @var string
     * @Column(nullable=true)
     */
    private $type;

    public function __construct () {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return User
     */
    public function getUser () {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser ( $user ) {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getContent () {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent ( $content ) {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getType () {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType ( $type ) {
        $this->type = $type;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt () {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt ( $updatedAt ) {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return string
     */
    public abstract function getUrl();

    /**
     * @param   EntityVisitor $visitor
     * @return  mixed
     */
    public function accept ( EntityVisitor $visitor ) {
        return $visitor->visitUserImage( $this );
    }
}