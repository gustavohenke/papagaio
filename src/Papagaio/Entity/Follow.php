<?php

namespace Papagaio\Entity;
use Papagaio\Entity\Visitor\EntityVisitor;

/**
 * Entidade Follow
 *
 * @Entity
 * @Table(name="followers")
 * @EntityListeners({
 *      "Papagaio\Model\Event\SaveFollowNotificationEvent",
 *      "Papagaio\Model\Event\RemoveFollowNotificationEvent",
 *      "Papagaio\Model\Event\FollowEmailEvent"
 * })
 * @package Papagaio\Entity
 */
class Follow implements Entity {

    /**
     * @Id
     * @Column(type="guid")
     * @GeneratedValue(strategy="UUID")
     * @var string
     */
    private $id;

    /**
     * @var User
     * @ManyToOne(targetEntity="User", inversedBy="followers")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var User
     * @ManyToOne(targetEntity="User", inversedBy="following")
     * @JoinColumn(name="follower_id", referencedColumnName="id")
     */
    private $follower;

    /**
     * @var \DateTime
     * @Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @return string
     */
    public function getId () {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId ( $id ) {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getUser () {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser ( $user ) {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getFollower () {
        return $this->follower;
    }

    /**
     * @param User $follower
     */
    public function setFollower ( $follower ) {
        $this->follower = $follower;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt () {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt ( $createdAt ) {
        $this->createdAt = $createdAt;
    }

    /**
     * Aceita a visita na entidade.
     *
     * @param   EntityVisitor $visitor
     * @return  mixed
     */
    public function accept ( EntityVisitor $visitor ) {
        return $visitor->visitFollow( $this );
    }

}