<?php

namespace Papagaio\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Papagaio\Core\App;
use Papagaio\Entity\Visitor\EntityVisitor;

/**
 * Entidade Post
 *
 * @Entity
 * @Table(name="posts")
 * @package Papagaio\Entity
 */
class Post implements Entity {

    /**
     * @Id
     * @Column(type="bigint")
     * @GeneratedValue
     */
    private $id;

    /**
     * @var User
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="author_id", referencedColumnName="id", nullable=false)
     */
    private $author;

    /**
     * @var ArrayCollection
     * @OneToMany(targetEntity="PostPhoto", mappedBy="post", cascade={"persist", "remove"})
     */
    private $photos;

    /**
     * @var string
     * @Column(nullable=true)
     */
    private $text;

    /**
     * @var array
     * @Column(type="json_array", nullable=true)
     */
    private $location;

    /**
     * @var \DateTime
     * @Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var ArrayCollection
     * @ManyToMany(targetEntity="HashTag")
     * @JoinTable(
     *  name="post_hashtags",
     *  joinColumns={@JoinColumn(name="post_id", referencedColumnName="id")},
     *  inverseJoinColumns={@JoinColumn(name="hashtag_id", referencedColumnName="id")}
     * )
     */
    private $hashTags;

    /**
     * @var Post
     * @ManyToOne(targetEntity="Post", inversedBy="replies")
     * @JoinColumn(name="reply_to", referencedColumnName="id")
     */
    private $replyTo;

    /**
     * @var ArrayCollection
     * @OneToMany(targetEntity="Post", mappedBy="replyTo")
     */
    private $replies;

    /**
     * @var ArrayCollection
     * @OneToMany(targetEntity="Mention", mappedBy="post")
     */
    private $mentions;

    /**
     * @var ArrayCollection
     * @OneToMany(targetEntity="Like", mappedBy="post")
     */
    private $likes;

    /**
     * Inicializa o Post
     */
    public function __construct () {
        $this->photos = new ArrayCollection();
        $this->hashTags = new ArrayCollection();
        $this->mentions = new ArrayCollection();
        $this->likes = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId () {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId ( $id ) {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getAuthor () {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor ( $author ) {
        $this->author = $author;
    }

    /**
     * @return string
     */
    public function getText () {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText ( $text ) {
        $this->text = $text;
    }

    /**
     * @return array
     */
    public function getLocation () {
        return $this->location;
    }

    /**
     * @param array $location
     */
    public function setLocation ( $location ) {
        $this->location = $location;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt () {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt ( $createdAt ) {
        $this->createdAt = $createdAt;
    }

    /**
     * @return ArrayCollection
     */
    public function getPhotos () {
        return $this->photos;
    }

    /**
     * @param ArrayCollection $photos
     */
    public function setPhotos ( $photos ) {
        $this->photos = $photos;
    }

    /**
     * @return  ArrayCollection
     */
    public function getHashTags () {
        return $this->hashTags;
    }

    /**
     * @param   ArrayCollection $hashTags
     */
    public function setHashTags ( $hashTags ) {
        $this->hashTags = $hashTags;
    }

    /**
     * @return Post
     */
    public function getReplyTo () {
        return $this->replyTo;
    }

    /**
     * @param Post $replyTo
     */
    public function setReplyTo ( $replyTo ) {
        $this->replyTo = $replyTo;
    }

    /**
     * @return ArrayCollection
     */
    public function getReplies () {
        return $this->replies;
    }

    /**
     * @param ArrayCollection $replies
     */
    public function setReplies ( $replies ) {
        $this->replies = $replies;
    }

    /**
     * @return ArrayCollection
     */
    public function getMentions () {
        return $this->mentions;
    }

    /**
     * @param ArrayCollection $mentions
     */
    public function setMentions ( $mentions ) {
        $this->mentions = $mentions;
    }

    /**
     * @return ArrayCollection
     */
    public function getLikes () {
        return $this->likes;
    }

    /**
     * @param ArrayCollection $likes
     */
    public function setLikes ( $likes ) {
        $this->likes = $likes;
    }

    /**
     * Retorna a URL API para o post
     *
     * @return string
     */
    public function getUrl () {
        $app = App::getInstance();
        return $app->base() . $app->buildUrl( 'post', [
            'id' => $this->getId()
        ]);
    }

    /**
     * Retorna a URL HTML para o post
     *
     * @return string
     */
    public function getHtmlUrl () {
        $app = App::getInstance();
        return $app->base() . $app->buildUrl( 'postHTML', [
            'id' => $this->getId()
        ]);
    }

    /**
     * @param   EntityVisitor $visitor
     * @return  mixed
     */
    public function accept ( EntityVisitor $visitor ) {
        return $visitor->visitPost( $this );
    }
}