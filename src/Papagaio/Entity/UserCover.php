<?php

namespace Papagaio\Entity;

use Papagaio\Core\App;

/**
 * Entidade virtual UserCover
 *
 * @package Papagaio\Entity
 */
class UserCover extends UserImage {

    /**
     * @return string
     */
    public function getUrl () {
        $app = App::getInstance();
        return $app->base() . $app->buildUrl( 'usercover', [
            'user' => $this->getUser()->getUsername()
        ]);
    }
}