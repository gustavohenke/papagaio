<?php

namespace Papagaio\Entity;
use Papagaio\Core\App;
use Papagaio\Entity\Visitor\EntityVisitor;

/**
 * Entidade PostPhoto
 *
 * @Entity
 * @Table(name="post_photos")
 * @package Papagaio\Entity
 */
class PostPhoto implements Entity {

    /**
     * @var string
     * @Id
     * @Column(type="bigint")
     * @GeneratedValue
     */
    private $id;

    /**
     * @var string
     * @Column
     */
    private $type;

    /**
     * @Column(type="blob")
     */
    private $data;

    /**
     * @var Post
     * @ManyToOne(targetEntity="Post", inversedBy="photos")
     * @JoinColumn(name="post_id", referencedColumnName="id", nullable=false)
     */
    private $post;

    /**
     * @return mixed
     */
    public function getId () {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId ( $id ) {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getType () {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType ( $type ) {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getData () {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData ( $data ) {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getPost () {
        return $this->post;
    }

    /**
     * @param mixed $post
     */
    public function setPost ( $post ) {
        $this->post = $post;
    }

    /**
     * Retorna a URL da imagem
     * @return string
     */
    public function getUrl () {
        $app = App::getInstance();
        return $app->base() . $app->buildUrl( 'postphoto', [
            'post' => $this->post->getId(),
            'photo' => $this->id
        ]);
    }

    /**
     * @param   EntityVisitor $visitor
     * @return  mixed
     */
    public function accept ( EntityVisitor $visitor ) {
        return $visitor->visitPostPhoto( $this );
    }
}