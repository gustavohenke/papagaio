<?php

namespace Papagaio\Entity;
use Papagaio\Entity\Visitor\EntityVisitor;

/**
 * Entidade Session
 *
 * @Entity
 * @Table(name="sessions")
 * @package Papagaio\Entity
 */
class Session implements Entity {

    /**
     * @Id
     * @Column(type="guid")
     * @GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="User")
     */
    private $user;

    /**
     * @Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @return  string
     */
    public function getId () {
        return $this->id;
    }

    /**
     * @param   string $id
     */
    public function setId ( $id ) {
        $this->id = $id;
    }

    /**
     * @return  User
     */
    public function getUser () {
        return $this->user;
    }

    /**
     * @param   User $user
     */
    public function setUser ( User $user ) {
        $this->user = $user;
    }

    /**
     * @return  \DateTime
     */
    public function getCreatedAt () {
        return $this->createdAt;
    }

    /**
     * @param   \DateTime $createdAt
     */
    public function setCreatedAt ( \DateTime $createdAt ) {
        $this->createdAt = $createdAt;
    }

    /**
     * @param   EntityVisitor $visitor
     * @return  array|null
     */
    public function accept ( EntityVisitor $visitor ) {
        return $visitor->visitSession( $this );
    }
}