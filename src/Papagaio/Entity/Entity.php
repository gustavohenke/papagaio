<?php

namespace Papagaio\Entity;

use Papagaio\Entity\Visitor\EntityVisitor;

interface Entity {

    /**
     * Aceita a visita na entidade.
     *
     * @param   EntityVisitor $visitor
     * @return  mixed
     */
    public function accept ( EntityVisitor $visitor );

}