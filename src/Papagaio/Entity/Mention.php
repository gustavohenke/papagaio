<?php

namespace Papagaio\Entity;
use Papagaio\Entity\Visitor\EntityVisitor;

/**
 * Entidade Mention
 *
 * @Entity
 * @Table(name="mentions")
 * @EntityListeners({
 *      "Papagaio\Model\Event\MentionEmailEvent",
 *      "Papagaio\Model\Event\SaveMentionNotificationEvent"
 * })
 * @package Papagaio\Entity
 */
class Mention implements Entity {

    /**
     * @var string
     * @Id
     * @Column(type="bigint")
     * @GeneratedValue
     */
    private $id;

    /**
     * @var User
     * @ManyToOne(targetEntity="User")
     */
    private $user;

    /**
     * @var Post
     * @ManyToOne(targetEntity="Post", inversedBy="mentions")
     */
    private $post;

    /**
     * @var \DateTime
     * @Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * @return string
     */
    public function getId () {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId ( $id ) {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getUser () {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser ( $user ) {
        $this->user = $user;
    }

    /**
     * @return Post
     */
    public function getPost () {
        return $this->post;
    }

    /**
     * @param Post $post
     */
    public function setPost ( $post ) {
        $this->post = $post;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt () {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt ( $createdAt ) {
        $this->createdAt = $createdAt;
    }

    /**
     * Aceita a visita na entidade.
     *
     * @param   EntityVisitor $visitor
     * @return  mixed
     */
    public function accept ( EntityVisitor $visitor ) {
        return $visitor->visitMention( $this );
    }
}