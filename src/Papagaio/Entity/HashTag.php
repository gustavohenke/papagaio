<?php

namespace Papagaio\Entity;

use Papagaio\Core\App;
use Papagaio\Entity\Visitor\EntityVisitor;

/**
 * Entidade HashTag
 *
 * @Entity
 * @Table(name="hashtags")
 * @package Papagaio\Entity
 */
class HashTag implements Entity {

    /**
     * @Id
     * @Column(type="string")
     * @var string
     */
    private $id;

    /**
     * @var \DateTime
     * @Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @return mixed
     */
    public function getId () {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId ( $id ) {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt () {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt ( $createdAt ) {
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getPostsUrl () {
        $app = App::getInstance();
        return $app->base() . $app->buildUrl( 'hashtagPosts', [
            'id' => $this->getId(),
        ]);
    }

    /**
     * @return string
     */
    public function getUrl () {
        $app = App::getInstance();
        return $app->base() . $app->buildUrl( 'hashtag', [
            'id' => $this->getId(),
        ]);
    }

    /**
     * Aceita a visita na entidade.
     *
     * @param   EntityVisitor $visitor
     * @return  mixed
     */
    public function accept ( EntityVisitor $visitor ) {
        return $visitor->visitHashTag( $this );
    }
}