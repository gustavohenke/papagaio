<?php

namespace Papagaio\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Papagaio\Entity\Follow;
use Papagaio\Entity\Like;
use Papagaio\Entity\Mention;
use Papagaio\Entity\Notification;
use Papagaio\Entity\User;
use Papagaio\Exception\ValidationException;

class NotificationModel extends Model {

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $notifRepo;

    public function __construct () {
        parent::__construct();
        $this->notifRepo = $this->em->getRepository( Notification::class );
    }

    /**
     * Cria uma notificação e retorna ela.
     *
     * @param   Notification $notification  A notificação para criar
     * @return  Notification                A notificação criada
     * @throws  ValidationException         Se algum dos campos foi preenchido incorretamente
     */
    public function create ( Notification $notification ) {
        if ( !in_array( $notification->getType(), Notification::$allowed_types ) ) {
            $msg = 'O tipo da notificação deve ser um dos tipos permitidos: ';
            $msg .= join( ', ', Notification::$allowed_types );
            throw new ValidationException( $msg );
        } else if ( empty( $notification->getRelationId() ) ) {
            throw new ValidationException( "O ID da relação da notificação deve ser fornecido!" );
        }

        $notification->setUnread( true );
        $notification->setCreatedAt( new \DateTime() );
        $this->em->persist( $notification );
        $this->em->flush();

        return $notification;
    }

    /**
     * Marca todas as notificações de um usuário como lidas.
     *
     * @param   User $user
     * @return  Collection
     */
    public function markAllAsRead ( User $user ) {
        $unread = $this->notifRepo->findBy([
            'user'      => $user,
            'unread'    => true
        ]);

        /** @var Notification $notification */
        foreach ( $unread as $notification ) {
            $notification->setUnread( false );
            $this->em->persist( $notification );
        }

        $this->em->flush();
        return new ArrayCollection( $unread );
    }

    /**
     * Exclui uma notificação
     *
     * @param   Notification $notification
     */
    public function remove ( Notification $notification ) {
        if ( !$notification ) {
            return;
        }

        $this->em->remove( $notification );
        $this->em->flush();
    }

    /**
     * Lista todas notificações de um usuário
     *
     * @param   User $user
     * @return  Collection
     */
    public function find ( User $user ) {
        $criteria = Criteria::create();
        $criteria->where( $criteria->expr()->eq( 'user', $user ) );
        $criteria->orderBy([ 'createdAt' => Criteria::DESC ]);

        return new ArrayCollection( $this->notifRepo->matching( $criteria ) );
    }

    /**
     * Encontra uma notificação usando a combinação de chave única da notificação (tipo, ID, usuário)
     *
     * @param   string $type
     * @param   string $id
     * @param   User $user
     * @return  Notification
     */
    public function findByUniqueKey ( $type, $id, User $user ) {
        var_dump($type, $id, $user->getId());
        $notification = $this->notifRepo->findOneBy([
            'type'          => $type,
            'relationId'    => $id,
            'user'          => $user
        ]);

        return $notification;
    }

    /**
     * Encontra o objeto relacionado de uma notificação.
     *
     * @param   Notification $notification
     * @return  object
     */
    public function findRelatedObject ( Notification $notification ) {
        /** @var EntityRepository $repo */
        $repo = null;

        switch ( $notification->getType() ) {
            case Notification::TYPE_MENTION:
                $repo = $this->em->getRepository( Mention::class );
                break;

            case Notification::TYPE_LIKE:
                $repo = $this->em->getRepository( Like::class );
                break;

            case Notification::TYPE_FOLLOW:
                $repo = $this->em->getRepository( Follow::class );
                break;
        }

        if ( $repo ) {
            return $repo->find( $notification->getRelationId() );
        }

        return null;
    }

    /**
     * Encontra o usuário que causou uma notificação.
     *
     * @param   Notification $notification
     * @return  User
     */
    public function findSender ( Notification $notification ) {
        $object = $this->findRelatedObject( $notification );

        switch ( $notification->getType() ) {
            case Notification::TYPE_MENTION:
                /** @var Mention $object */
                return $object->getPost()->getAuthor();

            case Notification::TYPE_LIKE:
                /** @var Like $object */
                return $object->getUser();

            case Notification::TYPE_FOLLOW:
                /** @var Follow $object */
                return $object->getFollower();
        }

        return null;
    }

}