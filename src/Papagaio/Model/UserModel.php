<?php

namespace Papagaio\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Papagaio\Core\DatabaseManager;
use Papagaio\Entity\Session;
use Papagaio\Entity\User;
use Papagaio\Entity\UserPhoto;
use Papagaio\Exception\ConflictException;
use Papagaio\Exception\NotFoundException;
use Papagaio\Exception\ValidationException;
use PasswordLib\PasswordLib;

class UserModel extends Model {

    /**
     * Expressão regular para validar nomes de usuário.
     *
     * @var string
     */
    const USERNAME_VALIDATION_REGEX = '/^[a-z0-9_.]+$/i';

    /**
     * @var PasswordLib
     */
    private static $encryptor;

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $userRepo;

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $sessionRepo;

    public function __construct () {
        if ( !self::$encryptor ) {
            self::$encryptor = new PasswordLib();
        }

        parent::__construct();

        $this->em = DatabaseManager::getInstance()->getEntityManager();
        $this->userRepo = $this->em->getRepository( User::class );
        $this->sessionRepo = $this->em->getRepository( Session::class );
    }

    // ---------------------------------------------------------------------------------------------
    // Operações de obter/listar usuários
    // ---------------------------------------------------------------------------------------------

    /**
     * Retorna todos os usuários da base
     *
     * @return Collection
     */
    public function findAll () {
        return new ArrayCollection( $this->userRepo->findAll() );
    }

    /**
     * Retorna todos os usuários com um nome de usuário parecido, ou que com o nome passado.
     *
     * @param   {string} $username
     * @return  Collection
     */
    public function findAllByUsername ( $username ) {
        // Escapa wildcards, para que não permita uma espécie de SQL Injection.
        // Além disso, também iremos trabalhar a pesquisa de forma case insensitive, então é
        // melhor converter para lowercase.
        $username = addcslashes( $username, '%' );
        $username = strtolower( $username );

        if ( strpos( $username, '@' ) === 0 ) {
            $username = substr( $username, 1 ) . '%';
        } else {
            $username = "%{$username}%";
        }

        $query = $this->em->createQueryBuilder();
        $query->select( 'u' )->from( User::class, 'u' );
        $query->where( 'LOWER(u.username) LIKE :username' );
        $query->setParameter( 'username', $username );
        return new ArrayCollection( $query->getQuery()->getResult() );
    }

    /**
     * Obtem um usuário pelo seu ID ou login
     *
     * @param   int|string $query
     * @return  User
     * @throws  NotFoundException   Quando o usuário não existe
     */
    public function find ( $query ) {
        if ( is_numeric( $query ) ) {
            $user = $this->userRepo->find( $query );
        } else {
            $user = $this->userRepo->findOneByUsername( $query );
        }

        if ( $user == null ) {
            throw new NotFoundException( "Usuário não encontrado" );
        }

        return $user;
    }

    /**
     * Encontra um usuário por um ID de uma sessão
     *
     * @param   {String} $id        O ID da sessão
     * @return  User
     * @throws  NotFoundException   Quando a sessão não existe
     */
    public function findBySession ( $id ) {
        /** @var Session $session */
        $session = $this->sessionRepo->find( $id );
        if ( $session == null ) {
            throw new NotFoundException( "Sessão inexistente" );
        }

        return $session->getUser();
    }

    // ---------------------------------------------------------------------------------------------
    // Operações de criar/alterar usuários
    // ---------------------------------------------------------------------------------------------

    /**
     * Cria um usuário e retorna-o.
     *
     * @param   User $user          O usuário a ser criado
     * @return  User                O usuário criado
     * @throws  ConflictException   Se o usuário e/ou e-mail já existir.
     * @throws  ValidationException Se o nome de usuário tiver caracteres inválidos
     */
    public function create ( User $user ) {
        $this->checkUsername( $user );
        $this->checkUserConflict( $user );
        $pw = self::$encryptor->createPasswordHash( $user->getPassword() );

        $user->setId( null );
        $user->setPassword( $pw );
        $user->setCreatedAt( new \DateTime() );
        $user->setPhoto( new UserPhoto() );

        $this->em->persist( $user );
        $this->em->flush();

        return $user;
    }

    /**
     * Atualiza um usuário
     *
     * @param   int|string $query
     * @param   User $user
     * @return  User
     * @throws  ConflictException   Se o usuário e/ou e-mail já existir.
     * @throws  ValidationException Se o nome de usuário tiver caracteres inválidos
     */
    public function update ( $query, User $user ) {
        $persisted = $this->find( $query );

        $user->setId( $persisted->getId() );
        $this->checkUsername( $user );
        $this->checkUserConflict( $user );

        $persisted->setUsername( $user->getUsername() );
        $persisted->setEmail( $user->getEmail() );

        // Se a senha foi fornecida, vamos atualizar ela, encriptando-a antes
        if ( !empty( $user->getPassword() ) ) {
            $pw = self::$encryptor->createPasswordHash( $user->getPassword() );
            $persisted->setPassword( $pw );
        }

        // Se a foto do usuário foi fornecida, vamos atualizar apenas o conteúdo dela
        if ( !empty( $user->getPhoto() ) ) {
            $photo = $persisted->getPhoto();
            $photo->setContent( $user->getPhoto()->getContent() );
            $photo->setType( $user->getPhoto()->getType() );
            $photo->setUpdatedAt( new \DateTime() );
        }

        $this->em->persist( $persisted );
        $this->em->flush();

        return $persisted;
    }

    /**
     * Verifica caracteres usados em um nome de usuário
     *
     * @param   User $user
     * @throws  ValidationException
     */
    private function checkUsername ( User $user ) {
        if ( preg_match( self::USERNAME_VALIDATION_REGEX, $user->getUsername() ) ) {
            return;
        }

        throw new ValidationException( 'O nome de usuário deve ter apenas caracteres A-Z, 0-9, _ e pontos' );
    }

    /**
     * Verifica conflito entre nome de usuário e e-mail.
     *
     * @param   User $user
     * @throws  ConflictException
     */
    private function checkUserConflict ( User $user ) {
        // Cria condição de pesquisa por usuário/email conflitantes com o que está sendo criado
        $criteria = Criteria::create();
        $criteria->where( Criteria::expr()->eq( 'username', $user->getUsername() ) )
                 ->orWhere( Criteria::expr()->eq( 'email', $user->getEmail() ) );

        // Ignora o usuário atual se ele já tiver sido persistido
        if ( !empty( $user->getId() ) ) {
            $criteria->andWhere( Criteria::expr()->neq( 'id', $user->getId() ) );
        }

        /** @var Collection $conflicts */
        $conflicts = $this->userRepo->matching( $criteria );
        if ( !$conflicts->isEmpty() ) {
            // Se o usuário é igual, então o conflito está aqui.
            // Senão, o conflito é com o e-mail.
            if ( $conflicts[ 0 ]->getUsername() === $user->getUsername() ) {
                $msg = sprintf( "Usuário com o nome %s já existe", $user->getUsername() );
            } else {
                $msg = sprintf( "Usuário com o e-mail %s já existe", $user->getEmail() );
            }

            throw new ConflictException( $msg );
        }
    }

    // ---------------------------------------------------------------------------------------------
    // Operações de login
    // ---------------------------------------------------------------------------------------------

    /**
     * Realiza o login de um usuário.
     *
     * @param   User $user
     * @return  bool|Session
     */
    public function login ( User $user ) {
        $user = $this->tempLogin( $user );
        if ( !$user ) {
            return false;
        }

        $session = new Session();
        $session->setUser( $user );
        $session->setCreatedAt( new \DateTime() );

        $this->em->persist( $session );
        $this->em->flush();

        return $session;
    }

    /**
     * Realiza o logout de uma sessão de um usuário
     *
     * Se os usuário dono da sessão não for o mesmo que o usuário que está tentando exclui-la,
     * este método não fará nada.
     *
     * @param   User $user          O usuário dono da sessão
     * @param   string $session_id  O ID da sessão para excluir
     * @throws  NotFoundException   Quando a sessão não existir ou não for do usuário logado.
     */
    public function logout ( User $user, $session_id ) {
        /** @var Session $session */
        $session = $this->sessionRepo->find( $session_id );
        if ( $session == null || $session->getUser()->getId() !== $user->getId() ) {
            throw new NotFoundException( 'Sessão não encontrada.' );
        }

        $this->em->remove( $session );
        $this->em->flush();
    }

    /**
     * Realiza um login temporário, que não cria sessões no banco.
     *
     * @param   User $user
     * @return  bool|User
     */
    public function tempLogin ( User $user ) {
        /** @var User $persisted_user */
        $persisted_user = $this->userRepo->findOneByUsername( $user->getUsername() );
        if ( $persisted_user == null ) {
            return false;
        }

        // Verifica se senha digitada confere com a senha armazenada
        $result = self::$encryptor->verifyPasswordHash(
            $user->getPassword(),
            $persisted_user->getPassword()
        );

        return $result ? $persisted_user : false;
    }

    // ---------------------------------------------------------------------------------------------
    // Operações de sessão
    // ---------------------------------------------------------------------------------------------

    /**
     * Retorna uma sessão
     *
     * @param   User $user
     * @param   string $session_id
     * @return  Session
     * @throws  NotFoundException
     */
    public function findSession ( User $user, $session_id ) {
        /** @var Session $session */
        $session = $this->sessionRepo->find( $session_id );
        if ( $session == null || $session->getUser()->getId() !== $user->getId() ) {
            throw new NotFoundException( "Sessão não encontrada" );
        }

        return $session;
    }

}