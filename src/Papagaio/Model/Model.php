<?php

namespace Papagaio\Model;

use Papagaio\Core\DatabaseManager;

abstract class Model {

    private static $cache = [];

    /**
     * @param   string $name    O nome do model desejado.
     * @return  Model
     * @throws  \Exception      Caso o Model desejado não exista.
     */
    public static function createModel ( $name ) {
        if ( isset( self::$cache[ $name ] ) ) {
            return self::$cache[ $name ];
        }

        $model = ucfirst( $name ) . 'Model';
        $cls = '\Papagaio\Model\\' . $model;

        if ( class_exists( $cls ) ) {
            self::$cache[ $name ] = new $cls();
            return self::$cache[ $name ];
        }

        throw new \Exception( "Model ${name} não existe!" );
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    public function __construct () {
        $this->em = DatabaseManager::getInstance()->getEntityManager();
    }

}