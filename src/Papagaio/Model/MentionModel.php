<?php

namespace Papagaio\Model;

use Papagaio\Core\DatabaseManager;
use Papagaio\Entity\Mention;
use Papagaio\Entity\Post;

class MentionModel extends Model {

    /**
     * @var UserModel
     */
    private $user;

    public function __construct () {
        parent::__construct();
        $this->user = Model::createModel( 'user' );
    }

    /**
     * Identifica e cria menções dado um post
     *
     * @param   Post $post
     * @return  array
     */
    public function identifyAndCreate ( Post $post ) {
        $matches = [];
        preg_match_all( '/@([a-z0-9]+)/i', $post->getText(), $matches );

        // Se houver menções duplicadas no texto, vamos removê-las.
        $mentions = array_unique( $matches[ 1 ] );

        foreach ( $mentions as &$mention ) {
            $user = $this->user->find( $mention );

            // Obviamente, a menção para um usuário inexistente não deve dar em nada!
            if ( !$user ) {
                continue;
            }

            $mention = new Mention();
            $mention->setUser( $user );
            $mention->setPost( $post );
            $mention->setCreatedAt( new \DateTime() );
            $this->em->persist( $mention );
        }

        $this->em->flush();
        return $mentions;
    }

}