<?php

namespace Papagaio\Model;

use Papagaio\Core\App;
use Papagaio\Entity\UserPhoto;
use Papagaio\Exception\ValidationException;

class ImageModel extends Model {

    const EXTENSION = 'jpg';
    const MIMETYPE = 'image/jpeg';

    /**
     * Retorna o conteúdo de uma foto
     *
     * @param   UserPhoto $photo
     * @return  UserPhoto
     */
    public function findUserPhoto ( UserPhoto $photo ) {
        if ( $photo->getContent() != null ) {
            return $photo;
        }

        parent::__construct();
        $app = App::getInstance();
        $path = $app->config()->get( 'userPicture' );

        list( ,, $type ) = getimagesize( $path );
        $stream = fopen( $path, 'r' );

        $photo->setType( image_type_to_mime_type( $type ) );
        $photo->setContent( $stream );
        return $photo;
    }

    /**
     * Processa uma imagem para um tamanho específico
     *
     * @param   string|resource $contents   O conteúdo da imagem original
     * @param   int $width                  A largura alvo
     * @param   int $height                 A altura alvo
     * @param   int $quality                A qualidade da imagem (JPEG)
     * @return  string                      O conteúdo binário da nova imagem.
     */
    public function process ( $contents, $width = null, $height = null, $quality = 95 ) {
        if ( is_resource( $contents ) ) {
            $contents = stream_get_contents( $contents );
        }

        list( $width2, $height2 ) = getimagesizefromstring( $contents );
        if ( empty( $width ) && empty( $height ) ) {
            $width = $width2;
            $height = $height2;
        } else if ( empty( $width ) ) {
            $height_pct = $height / $height2;
            $width = $width2 * $height_pct;
        } else if ( empty( $height ) ) {
            $width_pct = $width / $width2;
            $height = $height2 * $width_pct;
        }

        $resized_img = imagecreatetruecolor( $width, $height );
        $orig_img = imagecreatefromstring( $contents );

        // http://stackoverflow.com/a/1533651/2083599
        imagecopyresampled( $resized_img, $orig_img, 0, 0, 0, 0, $width, $height, $width2, $height2 );
        $contents = $this->printImage( $resized_img, $quality );

        // Libera os recursos utilizados
        imagedestroy( $resized_img );
        imagedestroy( $orig_img );

        return $contents;
    }

    /**
     * Valida um conteúdo binário, verificando se é imagem.
     *
     * @param   string|resource $contents
     * @return  bool
     * @throws  ValidationException         Se a imagem não é válida
     */
    public function validate ( $contents ) {
        if ( is_resource( $contents ) ) {
            $contents = stream_get_contents( $contents );
        }

        $img = @imagecreatefromstring( $contents );
        if ( !$img ) {
            throw new ValidationException( 'Imagem inválida detectada' );
        }

        return true;
    }

    /**
     * Borra uma imagem
     *
     * @param   string|resource $contents   O conteúdo da imagem
     * @param   int $blur                   Quantidade de blur para aplicar
     * @return  string                      O conteúdo binário da nova imagem
     */
    public function blur ( $contents, $blur = 20 ) {
        if ( is_resource( $contents ) ) {
            $contents = stream_get_contents( $contents );
        }

        $img = imagecreatefromstring( $contents );

        // Infelizmente não é a melhor forma de resolver isso, mas como a função imageconvolution()
        // é bem mal documentada, então esta é a forma de fazermos...
        // Mais info: http://stackoverflow.com/questions/7245710/php-gd-better-gaussian-blur
        for ( $i = 0; $i < $blur; $i++ ) {
            imagefilter( $img, IMG_FILTER_GAUSSIAN_BLUR );
        }

        $contents = $this->printImage( $img );
        imagedestroy( $img );

        return $contents;
    }

    /**
     * Pega o conteúdo de um resource de imagem e retorna como string
     *
     * @param   resource $img   A imagem propriamente dita (resource do GD)
     * @param   int $quality    Qual a qualidade JPEG
     * @return  string          O conteúdo binário da imagem
     */
    private function printImage ( $img, $quality = 95 ) {
        if ( $quality == null ) {
            $quality = 95;
        }

        // http://stackoverflow.com/a/2207919/2083599
        ob_start();
        imagejpeg( $img, null, $quality );
        $contents = ob_get_contents();
        ob_end_clean();

        return $contents;
    }

}