<?php

namespace Papagaio\Model;

use Papagaio\Entity\Like;
use Papagaio\Entity\Post;
use Papagaio\Entity\User;
use Papagaio\Exception\NotFoundException;
use Papagaio\Exception\ValidationException;

class LikeModel extends Model {

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repo;

    public function __construct () {
        parent::__construct();
        $this->repo = $this->em->getRepository( Like::class );
    }

    /**
     * Obtem uma curtida em um post
     *
     * @param   Post $post
     * @param   User $user
     * @return  Like
     * @throws  NotFoundException
     */
    public function find ( Post $post, User $user ) {
        $like = $this->repo->findOneBy([
            'user' => $user,
            'post' => $post
        ]);

        if ( !$like ) {
            throw new NotFoundException( 'Curtida não encontrada!' );
        }

        return $like;
    }

    /**
     * Adiciona uma curtida à um post
     *
     * @param   Post $post
     * @param   User $user
     * @return  Like
     * @throws  ValidationException
     */
    public function create ( Post $post, User $user ) {
        if ( $post == null || $user == null ) {
            throw new ValidationException();
        }

        $like = new Like();
        $like->setPost( $post );
        $like->setUser( $user );
        $like->setCreatedAt( new \DateTime() );

        $this->em->persist( $like );
        $this->em->flush();

        return $like;
    }

    public function remove ( Post $post, User $user ) {
        if ( $post == null || $user == null ) {
            throw new ValidationException();
        }

        $like = $this->find( $post, $user );
        $this->em->remove( $like );
        $this->em->flush();
    }

}