<?php

namespace Papagaio\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Papagaio\Entity\Follow;
use Papagaio\Entity\User;
use Papagaio\Exception\NotFoundException;

class FollowModel extends Model {

    /**
     * Obtem uma relação de seguidor
     *
     * @param   User $user
     * @param   User $follower
     * @return  Follow
     * @throws  NotFoundException   Se $follower não seguir $user
     */
    public function find ( User $user, User $follower ) {
        $result = $user->getFollowers()->filter(function ( Follow $follow ) use ( $follower ) {
            return $follow->getFollower() == $follower;
        });

        if ( $result->isEmpty() ) {
            $msg = sprintf(
                'O usuário @%s não segue o usuário @%s',
                $follower->getUsername(),
                $user->getUsername()
            );
            throw new NotFoundException( $msg );
        }

        return $result->first();
    }

    /**
     * Faz um usuário seguir outro
     *
     * @param   User $follower  O usuário seguidor
     * @param   User $followed  O usuário seguido
     */
    public function follow ( User $follower, User $followed ) {
        try {
            if ( $follower == $followed ) {
                return;
            }

            $this->find( $followed, $follower );
        } catch ( NotFoundException $e ) {
            $follow = new Follow();
            $follow->setUser( $followed );
            $follow->setFollower( $follower );
            $follow->setCreatedAt( new \DateTime() );

            $this->em->persist( $follow );
            $this->em->flush();
        }
    }

    /**
     * Faz um usuário parar de seguir outro
     *
     * @param   User $follower  O usuário seguidor
     * @param   User $followed  O usuário seguido
     */
    public function unfollow ( User $follower, User $followed ) {
        try {
            if ( $follower == $followed ) {
                return;
            }

            $follow = $this->find( $followed, $follower );
            $this->em->remove( $follow );
            $this->em->flush();
        } catch ( NotFoundException $e ) {}
    }

    /**
     * Mapeia uma lista de seguidores para usuários
     *
     * @param   User $user
     * @return  ArrayCollection
     */
    public function mapFollowersToUsers ( User $user ) {
        return $user->getFollowers()->map(function ( Follow $follow ) {
            return $follow->getFollower();
        });
    }

    /**
     * Mapeia uma lista de seguindo para usuários
     *
     * @param   User $user
     * @return  ArrayCollection
     */
    public function mapFollowingToUsers ( User $user ) {
        return $user->getFollowing()->map(function ( Follow $follow ) {
            return $follow->getUser();
        });
    }

}