<?php

namespace Papagaio\Model\Report;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Papagaio\Entity\Post;
use Papagaio\Entity\User;
use Papagaio\Model\FollowModel;
use Papagaio\Model\Model;
use Papagaio\Model\PostModel;
use Papagaio\Model\UserModel;

class UserReportModel extends Model {

    /**
     * @var UserModel
     */
    private $userModel;

    /**
     * @var PostModel
     */
    private $postModel;

    /**
     * @var FollowModel
     */
    private $followModel;

    /**
     * Mapa de peso pra cada usuário durante esta execução do relatório.
     *
     * @var array
     */
    private $cogencyMap;

    public function __construct () {
        parent::__construct();

        $this->userModel = static::createModel( 'user' );
        $this->postModel = static::createModel( 'post' );
        $this->followModel = static::createModel( 'follow' );
    }

    /**
     * Encontra usuários sugeridos
     *
     * @param   User $user
     * @return  Collection
     */
    public function execute ( User $user ) {
        $users = $this->userModel->findAll();
        $usersArr = $users->toArray();

        $keys = array_map( [ $this, 'getUsername' ], $usersArr );
        $values = array_map( [ $this, 'calculateCogency' ], $usersArr );
        $this->cogencyMap = array_combine( $keys, $values );

        // Nós precisamos manter apenas os usuários que tem um peso de sugestão não-zero,
        // e aqueles que já não são seguidos pelo usuário passado.
        $this->cogencyMap = array_filter( $this->cogencyMap, [ $this, 'removeZeroCogency' ]);
        uasort( $this->cogencyMap, [ $this, 'compareUsers' ] );

        $usernames = array_keys( $this->cogencyMap );
        $usernames = array_filter( $usernames, function ( $username ) use ( $user ) {
            $following = $this->followModel->mapFollowingToUsers( $user )->toArray();
            $following = array_map( [ $this, 'getUsername' ], $following );

            return $username !== $user->getUsername() && !in_array( $username, $following );
        });

        $users = $users->filter(function ( User $user ) use ( $usernames ) {
            return in_array( $user->getUsername(), $usernames );
        });

        // Precisamos criar uma nova coleção apenas com os valores pois senão são mantidos os
        // indices dos itens da coleção anterior (que pode ser qualquer número >= 1)
        return new ArrayCollection( $users->getValues() );
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Função callback para ordenação dos usuários de acordo com seus pesos.
     *
     * @param   int $cogency1
     * @param   int $cogency2
     * @return  int
     */
    private function compareUsers ( $cogency1, $cogency2 ) {
        return $cogency1 < $cogency2 ? -1 : ( $cogency1 == $cogency2 ? 0 : 1 );
    }

    /**
     * Retorna o login de um usuário
     *
     * @param   User $user
     * @return  string
     */
    private function getUsername ( User $user ) {
        return $user->getUsername();
    }

    /**
     * Calcula o peso de um usuário.
     *
     * @param   User $user
     * @return  float
     */
    private function calculateCogency ( User $user ) {
        $cogency = $this->countLikes( $user ) * 0.5;
        $cogency += $this->countFollowers( $user ) * 0.2;

        return $cogency;
    }

    /**
     * Função callback para remover usuários com peso 0
     *
     * @param   int $cogency
     * @return  boolean
     */
    private function removeZeroCogency ( $cogency ) {
        return $cogency != 0;
    }

    /**
     * Conta número de likes nos posts de um usuário
     *
     * @param   User $user
     * @return  int
     */
    private function countLikes ( User $user ) {
        $posts = $this->postModel->findAllByUser( $user )->toArray();
        return array_reduce( $posts, function ( $val, Post $post ) {
            return $val + $post->getLikes()->count();
        }, 0 );
    }

    /**
     * Conta número de seguidores de um usuário
     *
     * @param   User $user
     * @return  int
     */
    private function countFollowers ( User $user ) {
        return $user->getFollowers()->count();
    }

}