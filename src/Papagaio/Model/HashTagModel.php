<?php

namespace Papagaio\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Papagaio\Core\DatabaseManager;
use Papagaio\Entity\HashTag;
use Papagaio\Exception\NotFoundException;

class HashTagModel extends Model {

    /**
     * Repository para HashTags
     *
     * @var     \Doctrine\ORM\EntityRepository
     */
    private $hashTagRepo;

    public function __construct () {
        parent::__construct();
        $this->hashTagRepo = $this->em->getRepository( HashTag::class );
    }

    /**
     * Lista todas as hashtags
     *
     * @return  Collection
     */
    public function findAll () {
        return new ArrayCollection( $this->hashTagRepo->findAll() );
    }

    /**
     * Lista todas as hashtags com ID parecido
     *
     * @param   string $id
     * @return  Collection
     */
    public function findAllById ( $id ) {
        // Escapa wildcard, pra garantir que ele não vá ser usado na pesquisa.
        // Além disso, nossas pesquisa serão case insensitive, então é melhor usar tudo lowercase.
        $id = addcslashes( $id, '%' );
        $id = strtolower( $id );

        if ( strpos( $id, '#' ) === 0 ) {
            $id = substr( $id, 1 ) . '%';
        } else {
            $id = "%{$id}%";
        }

        $query = $this->em->createQueryBuilder();
        $query->select( 'h' )->from( HashTag::class, 'h' );
        $query->where( 'LOWER(h.id) LIKE :id' );
        $query->setParameter( 'id', $id );
        return new ArrayCollection( $query->getQuery()->getResult() );
    }

    /**
     * Obtem uma hash tag
     *
     * @param   string $id
     * @return  HashTag
     * @throws  NotFoundException   Quando a hash tag não existir
     */
    public function find ( $id ) {
        $tag = $this->hashTagRepo->find( strtolower( $id ) );
        if ( !$tag ) {
            throw new NotFoundException( "Hash Tag não encontrada" );
        }

        return $tag;
    }

    /**
     * Conta quantos posts usam uma hash tag
     *
     * @param   HashTag $tag
     * @return  int
     */
    public function countPosts ( HashTag $tag ) {
        /** @var PostModel $post_model */
        $post_model = Model::createModel( 'post' );
        return $post_model->findAllByHashTag( $tag )->count();
    }

    /**
     * Identifica e cria hashtags a partir de uma string
     *
     * @param   string $text    Texto para extrair e criar hash tags
     * @return  array           Lista de hashtags encontradas
     */
    public function identifyAndCreate ( $text ) {
        $matches = [];
        preg_match_all( '/#([a-z0-9]+)/i', $text, $matches );

        // Se houver tags duplicadas no texto, vamos removê-las.
        $tags = array_unique( $matches[ 1 ] );

        foreach ( $tags as &$tag ) {
            $name = strtolower( $tag );

            // Obviamente, não vamos criar a hashtag se ela já existir.
            $tag = $this->hashTagRepo->find( $name );
            if ( $tag != null ) {
                continue;
            }

            $tag = new HashTag();
            $tag->setId( $name );
            $tag->setCreatedAt( new \DateTime() );
            $this->em->persist( $tag );
        }

        $this->em->flush();
        return $tags;
    }

}