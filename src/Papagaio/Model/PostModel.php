<?php

namespace Papagaio\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Papagaio\Core\DatabaseManager;
use Papagaio\Entity\HashTag;
use Papagaio\Entity\Post;
use Papagaio\Entity\PostPhoto;
use Papagaio\Entity\User;
use Papagaio\Exception\BadRequestException;
use Papagaio\Exception\NotFoundException;
use Papagaio\Exception\ValidationException;

class PostModel extends Model {

    /**
     * Repository para Posts
     *
     * @var     \Doctrine\ORM\EntityRepository
     */
    private $postRepo;

    /**
     * Model para operações com HashTags
     * @var     HashTagModel
     */
    private $hashTag;

    /**
     * Model para operações com Mentions
     * @var     MentionModel
     */
    private $mention;

    /**
     * Model para operações com Imagens
     * @var     ImageModel
     */
    private $image;

    public function __construct () {
        parent::__construct();
        $this->postRepo = $this->em->getRepository( Post::class );
        $this->hashTag = Model::createModel( 'hashTag' );
        $this->image = Model::createModel( 'image' );
        $this->mention = Model::createModel( 'mention' );
    }

    /**
     * Lista todos posts.
     * Se um usuário tiver sido passado, serão retornados apenas os posts de todos a quem ele segue,
     * e os posts do usuário passado.
     *
     * @param   User $user
     * @return  Collection
     */
    public function findAll ( User $user ) {
        $criteria = Criteria::create();

        if ( $user != null ) {
            /** @var FollowModel $followModel */
            $followModel = static::createModel( 'follow' );
            $authors = $followModel->mapFollowingToUsers( $user );
            $authors->add( $user );

            $criteria->where( $criteria->expr()->in( 'author', $authors->toArray() ) );
        }

        $criteria->orderBy([ 'createdAt' => Criteria::DESC ]);
        $posts = $this->postRepo->matching( $criteria );
        return new ArrayCollection( $posts );
    }

    /**
     * Lista todos posts de um usuário
     *
     * @param   User $user
     * @return  Collection
     */
    public function findAllByUser ( User $user ) {
        $criteria = Criteria::create();
        $criteria->where( Criteria::expr()->eq( 'author', $user ) );
        $criteria->orderBy([ 'createdAt' => Criteria::DESC ]);

        return $this->postRepo->matching( $criteria );
    }

    /**
     * Lista todos posts de uma hashtag
     *
     * @param   HashTag $tag
     * @return  Collection
     */
    public function findAllByHashTag ( HashTag $tag ) {
        // Por enquanto, é necessário fazer este workaround até que a seguinte issue esteja resolvida:
        // http://www.doctrine-project.org/jira/browse/DDC-2988
        $query = $this->em->createQueryBuilder();
        $query->select( 'p' );
        $query->from( Post::class, 'p' );
        $query->join( 'p.hashTags', 'h', 'WITH', $query->expr()->in( 'h.id', [ $tag->getId() ]) );

        return new ArrayCollection( $query->getQuery()->getResult() );
    }

    /**
     * Retorna um post
     *
     * @param   int $id
     * @return  Post
     * @throws  NotFoundException   Quando o post não existe
     */
    public function find ( $id ) {
        $post = $this->postRepo->find( $id );
        if ( $post == null ) {
            throw new NotFoundException( "Post não encontrado" );
        }

        return $post;
    }

    /**
     * Cria um post
     *
     * @param   Post $post
     * @return  Post
     * @throws  ValidationException Se o texto do post for muito longo (mais que 150 caracteres)
     */
    public function create( Post $post ) {
        if ( strlen( $post->getText() ) > 150 ) {
            throw new ValidationException( 'Texto do post deve ter até 150 caracteres' );
        }

        $loc = $post->getLocation();
        if ( $loc ) {
            // Valida se temos lat/lng
            $this->validateLocation( $loc );

            // Removemos quaisquer sujeiras, pois queremos ficar apenas com lat/lng
            $post->setLocation( array_intersect_key( $loc, [
                'latitude'  => null,
                'longitude' => null
            ]));
        }

        // Identifica, cria e adiciona todas as hashtags do texto passado
        $tags = $this->hashTag->identifyAndCreate( $post->getText() );
        foreach ( $tags as $tag ) {
            $post->getHashTags()->add( $tag );
        }

        // Valida as fotos enviadas, verificando se são imagens mesmo
        $photos = $post->getPhotos();
        /** @var PostPhoto $photo */
        foreach ( $photos as $photo ) {
            $this->image->validate( $photo->getData() );
        }

        $post->setCreatedAt( new \DateTime() );
        $this->em->persist( $post );
        $this->em->flush();

        // Identifica, cria e adiciona todas as menções do post
        $mentions = $this->mention->identifyAndCreate( $post );
        foreach ( $mentions as $mention ) {
            $post->getMentions()->add( $mention );
        }

        return $post;
    }

    /**
     * Valida o objeto de localização, verificando se as propriedades obrigatórias
     * (latitude e longitude) estão presentes e seus valores estão dentro do range aceito.
     *
     * @param   array $loc
     * @throws  ValidationException
     */
    private function validateLocation ( array $loc ) {
        if ( !isset( $loc[ 'latitude' ] ) || !isset( $loc[ 'longitude' ] ) ) {
            throw new ValidationException( 'Localização inválida' );
        }

        list( $lat, $lng ) = [ $loc[ 'latitude' ], $loc[ 'longitude' ] ];
        if ( $lat < -90 || $lat > 90 ) {
            throw new ValidationException( 'Latitude deve estar entre -90° e +90°' );
        } else if ( $lng < -180 || $lng > 180 ) {
            throw new ValidationException( 'Longitude deve estar entre -180° e +180°' );
        }
    }

}