<?php

namespace Papagaio\Model\Event;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Papagaio\Core\App;
use Papagaio\Entity\Event\PostPersistEvent;
use Papagaio\Entity\Like;
use Papagaio\Entity\Notification;
use Papagaio\Model\NotificationModel;

class SaveLikeNotificationEvent implements PostPersistEvent {

    /**
     * @PostPersist
     * @param   Like $instance
     * @param   LifecycleEventArgs $args
     * @return  void
     */
    public function onPostPersist ( $instance, LifecycleEventArgs $args ) {
        // Não notifica o autor do post se quem curtiu é o próprio autor
        if ( $instance->getUser() == $instance->getPost()->getAuthor() ) {
            return;
        }

        $notification = new Notification();
        $notification->setType( Notification::TYPE_LIKE );
        $notification->setRelationId( $instance->getId() );
        $notification->setUser( $instance->getPost()->getAuthor() );

        $app = App::getInstance();

        /** @var NotificationModel $notificationModel */
        $notificationModel = $app->model( 'notification' );
        $notificationModel->create( $notification );
    }
}