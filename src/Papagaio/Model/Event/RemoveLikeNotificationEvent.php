<?php

namespace Papagaio\Model\Event;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Papagaio\Core\App;
use Papagaio\Entity\Event\PreRemoveEvent;
use Papagaio\Entity\Like;
use Papagaio\Entity\Notification;
use Papagaio\Model\NotificationModel;

class RemoveLikeNotificationEvent implements PreRemoveEvent {

    /**
     * @PreRemove
     * @param   Like $instance
     * @param   LifecycleEventArgs $args
     * @return  void
     */
    public function onPreRemove ( $instance, LifecycleEventArgs $args ) {
        $app = App::getInstance();

        /** @var NotificationModel $notificationModel */
        $notificationModel = $app->model( 'notification' );
        $notification = $notificationModel->findByUniqueKey(
            Notification::TYPE_LIKE,
            $instance->getId(),
            $instance->getPost()->getAuthor()
        );
        $notificationModel->remove( $notification );
    }

}