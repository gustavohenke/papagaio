<?php

namespace Papagaio\Model\Event;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Papagaio\Core\App;
use Papagaio\Entity\Event\PreRemoveEvent;
use Papagaio\Entity\Follow;
use Papagaio\Entity\Notification;
use Papagaio\Model\NotificationModel;

class RemoveFollowNotificationEvent implements PreRemoveEvent {

    /**
     * @PreRemove
     * @param   Follow $instance
     * @param   LifecycleEventArgs $args
     * @return  void
     */
    public function onPreRemove ( $instance, LifecycleEventArgs $args ) {
        $app = App::getInstance();

        /** @var NotificationModel $notificationModel */
        $notificationModel = $app->model( 'notification' );
        $notification = $notificationModel->findByUniqueKey(
            Notification::TYPE_FOLLOW,
            $instance->getId(),
            $instance->getUser()
        );
        $notificationModel->remove( $notification );
    }
}