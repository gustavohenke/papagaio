<?php

namespace Papagaio\Model\Event;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Papagaio\Core\App;
use Papagaio\Entity\Event\PostPersistEvent;
use Papagaio\Entity\Mention;
use Papagaio\Entity\Notification;
use Papagaio\Model\NotificationModel;

class SaveMentionNotificationEvent implements PostPersistEvent {

    /**
     * @PostPersist
     * @param   Mention $instance
     * @param   LifecycleEventArgs $args
     * @return  void
     */
    public function onPostPersist ( $instance, LifecycleEventArgs $args ) {
        $notification = new Notification();
        $notification->setType( Notification::TYPE_MENTION );
        $notification->setRelationId( $instance->getId() );
        $notification->setUser( $instance->getUser() );

        $app = App::getInstance();

        /** @var NotificationModel $notificationModel */
        $notificationModel = $app->model( 'notification' );
        $notificationModel->create( $notification );
    }
}