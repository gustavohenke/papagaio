<?php

namespace Papagaio\Model\Event;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Papagaio\Core\App;
use Papagaio\Entity\Event\PostPersistEvent;
use Papagaio\Entity\Follow;
use Papagaio\Model\EmailModel;

class FollowEmailEvent implements PostPersistEvent {

    /**
     * @PostPersist
     * @param   Follow $instance
     * @param   LifecycleEventArgs $args
     * @return  void
     */
    public function onPostPersist ( $instance, LifecycleEventArgs $args ) {
        $app = App::getInstance();

        /** @var EmailModel $emailModel */
        $emailModel = $app->model( 'email' );
        $emailModel->sendFollowNotification( $instance );
    }
}