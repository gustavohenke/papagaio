<?php

namespace Papagaio\Model\Event;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Papagaio\Core\App;
use Papagaio\Entity\Event\PostPersistEvent;
use Papagaio\Entity\Mention;
use Papagaio\Model\EmailModel;
use Papagaio\Utils\AsyncRequest;

class MentionEmailEvent implements PostPersistEvent {

    /**
     * @PostPersist
     * @param   Mention $instance
     * @param   LifecycleEventArgs $args
     * @return  void
     */
    public function onPostPersist ( $instance, LifecycleEventArgs $args ) {
        $app = App::getInstance();

        /** @var EmailModel $emailModel */
        $emailModel = $app->model( 'email' );
        $emailModel->sendMentionNotification( $instance );
    }
}