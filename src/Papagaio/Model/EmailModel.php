<?php

namespace Papagaio\Model;

use Papagaio\Core\App;
use Papagaio\Entity\Follow;
use Papagaio\Entity\Mention;
use Papagaio\Entity\User;
use Papagaio\Utils\Utils;
use Slim\Http\Util;

class EmailModel extends Model {

    const MENTION_TEMPLATE = 'mention';
    const FOLLOW_TEMPLATE = 'follow';

    /**
     * @var \Mandrill
     */
    private $mailer;

    /**
     * @var array
     */
    private $config;

    public function __construct () {
        parent::__construct();

        $app = App::getInstance();
        $this->config = $app->config()->get( 'email' );

        $this->mailer = new \Mandrill( $this->config[ 'key' ] );
        curl_setopt( $this->mailer->ch, CURLOPT_CAINFO, $app->dir() . '/ca-bundle.crt' );
    }

    public function sendMentionNotification ( Mention $mention ) {
        $vars = [
            'post' => $mention->getPost(),
            'user' => $mention->getUser()
        ];

        $rcpt = $this->buildRecipients([ $mention->getUser() ]);
        $this->send( self::MENTION_TEMPLATE, $rcpt, $vars );
    }

    public function sendFollowNotification ( Follow $follow ) {
        $vars = [
            'user' => $follow->getUser(),
            'follower' => $follow->getFollower()
        ];

        $rcpt = $this->buildRecipients([ $follow->getUser() ]);
        $this->send( self::FOLLOW_TEMPLATE, $rcpt, $vars );
    }

    /**
     * Cria um e-mail.
     *
     * @param   string $templateName
     * @param   array $recipients
     * @param   array $vars
     */
    private function send ( $templateName, array $recipients, array $vars ) {
        /** @var array $template */
        $template = $this->config[ 'templates' ][ $templateName ];

        $message = [
            'html'                  => $this->buildHTML( $template[ 'file' ] ),
            'subject'               => $template[ 'subject' ],
            'to'                    => $recipients,
            'from_name'             => $this->config[ 'from' ][ 'name' ],
            'from_email'            => $this->config[ 'from' ][ 'address' ],
            'inline_css'            => true,
            'tags'                  => $template[ 'tags' ],
            'important'             => false,
            'track_opens'           => false,
            'track_clicks'          => false,
            'auto_text'             => true,
            'preserve_recipients'   => true,
            'merge_language'        => 'handlebars',
            'global_merge_vars'     => $this->transformVars( $vars ),
            'subaccount'            => $this->config[ 'subaccount' ],
            'images'                => $this->getImages()
        ];

        $logger = App::getInstance()->log();
        $logger->info( array_diff_key(
            $message,
            [
                'html' => null,
                'global_merge_vars' => null,
                'images' => null
            ]
        ));

        $this->mailer->messages->send( $message, true );
    }

    private function buildRecipients ( array $users ) {
        $recipients = [];

        /** @var User $user */
        foreach ( $users as $user ) {
            $recipients[] = [
                'email' => $user->getEmail(),
                'name'  => '@' . $user->getUsername(),
                'type'  => 'to'
            ];
        }

        return $recipients;
    }

    /**
     * Transforma as variáveis passadas para um array aceito pelo Mandrill.
     * https://mandrillapp.com/api/docs/messages.php.html#method=send
     *
     * @param   array $vars
     * @return  array
     */
    private function transformVars ( $vars ) {
        $result = [];

        // Transformamos as variáveis passadas para JSON, exatamente no formato como é retornado
        // pela API.
        $vars = json_decode( Utils::toJSON( $vars ), true );

        foreach ( $vars as $name => $value ) {
            $result[] = [
                'name'      => $name,
                'content'   => $value
            ];
        }

        return $result;
    }

    /**
     * Dado o caminho de um template de e-mail em HTML, retorna o mesmo com seu CSS.
     *
     * @param   string $file
     * @return  string
     */
    private function buildHTML ( $file ) {
        $app = App::getInstance();
        $url = $app->base() . $app->buildUrl( 'asset', [
            'name' => $this->config[ 'css' ]
        ]);

        // Obtemos o CSS usando cURL, que garante que funcione no Heroku.
        // Dica retirada de http://www.widecodes.com/0zxjWUWPXU/heroku-php-filegetcontents-from-heroku-server-to-itself-causes-h12-request-timeout.html
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        $css = curl_exec( $ch );
        curl_close( $ch );

        $html = "<style>${css}</style>";
        $html .= file_get_contents( $file );
        return $html;
    }

    /**
     * Retorna uma lista de imagens que serão embutidas no e-mail sendo enviado.
     *
     * @return array
     */
    private function getImages () {
        $result = [];
        $images = $this->config[ 'images' ];
        foreach ( $images as $name => $path ) {
            $result[] = [
                'type'      => Utils::getMimeType( $path ),
                'name'      => $name,
                'content'   => base64_encode( file_get_contents( $path ) )
            ];
        }

        return $result;
    }

}