<?php

namespace Papagaio\Core;

class Config {

    /**
     * Instância singleton das configurações.
     *
     * @var Config
     */
    private static $instance;

    public static function getInstance () {
        if ( !isset( self::$instance ) ) {
            self::$instance = new Config();
        }

        return self::$instance;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Configurações do app.
     *
     * @var \Noodlehaus\Config
     */
    private $config;

    private function __construct () {
        $this->config = new \Noodlehaus\Config( getcwd() . '/config' );
    }

    /**
     * Obtem um valor de configuração
     *
     * @param  string $name
     * @return mixed
     */
    public function get ( $name ) {
        $value = $this->config->get( $name );

        if ( $this->has( 'env.' . $name ) ) {
            $env = $this->resolve( $this->get( 'env.' . $name ) );

            if ( is_array( $env ) && is_array( $value ) ) {
                return array_merge( $value, $env );
            }

            return $env ? $env : $value;
        }

        return $value;
    }

    /**
     * Define um valor de configuração
     *
     * @param string $name
     * @param mixed $value
     */
    public function set ( $name, $value ) {
        $this->config->set( $name, $value );
    }

    /**
     * Determina se uma configuração existe
     *
     * @param   string $name
     * @return  bool
     */
    public function has ( $name ) {
        return $this->config->get( $name ) != null;
    }

    /**
     * Resolve todas as variáveis de ambiente de uma string ou array.
     *
     * @param   string|array $env
     * @return  string|array
     */
    private function resolve ( $env ) {
        if ( is_array( $env ) ) {
            foreach ( $env as $key => &$value ) {
                $value = $this->resolve( $value );
                if ( $value == null ) {
                    unset( $env[ $key ] );
                }
            }

            return $env;
        }

        $resolved = getenv( $env );
        return $resolved ? $resolved : null;
    }

}