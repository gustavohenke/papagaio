<?php

namespace Papagaio\Core;

use Slim\Log;
use Slim\LogWriter;

class Logger extends LogWriter {

    public function __construct ( $name = 'app.log' ) {
        $config = Config::getInstance();
        $stream = fopen(
            $config->get( 'log.filename' ),
            $config->get( 'log.mode' )
        );

        parent::__construct( $stream );
    }

    public function write ( $message, $level ) {
        switch ( $level ) {
            case Log::FATAL:
                $label = 'FATAL';
                break;

            case Log::WARN:
                $label = 'WARN';
                break;

            case Log::ERROR:
                $label = 'ERROR';
                break;

            case Log::INFO:
                $label = 'INFO';
                break;

            default:
                $label = 'DEBUG';
                break;
        }

        $now = $this->getNow();
        $message = "[$label] $now - " . (string) $message;

        return parent::write( $message, $level );
    }

    /**
     * @return string
     */
    private function getNow () {
        $time = microtime( true );
        $micro = sprintf( '%06d', ( $time - floor( $time ) ) * 1000000 );
        $now = new \DateTime( date( 'Y-m-d H:i:s.' . $micro, $time ) );
        return $now->format( 'Y-m-d H:i:s.u' );
    }
}