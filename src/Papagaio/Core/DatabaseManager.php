<?php

namespace Papagaio\Core;

use Doctrine\Common\Cache\ApcCache;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

class DatabaseManager {

    /**
     * Instância singleton do DatabaseManager.
     *
     * @var DatabaseManager
     */
    private static $instance;

    public static function getInstance() {
        if ( !isset( self::$instance ) ) {
            self::$instance = new DatabaseManager();
        }

        return self::$instance;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var Config
     */
    private $config;

    private function __construct () {
        $this->config = Config::getInstance();
        $doctrine_cfg = Setup::createAnnotationMetadataConfiguration(
            $this->config->get( 'doctrine.paths' ),
            $this->config->get( 'doctrine.devMode' )
        );

        $this->loadCache( $doctrine_cfg );

        // Obtem as configurações do DB
        // Se for uma string, usa como URL.
        $db_cfg = $this->config->get( 'db' );
        if ( is_string( $db_cfg ) ) {
            $db_cfg = [ 'url' => $db_cfg ];
        }

        $this->entityManager = EntityManager::create( $db_cfg, $doctrine_cfg );
        $this->loadTypes();
    }

    /**
     * Carrega implementações de cache na configuração do Doctrine.
     *
     * Usa o APC
     *
     * @param   Configuration $config
     */
    private function loadCache ( Configuration $config ) {
        if ( !$this->config->get( 'cache.opcache' ) ) {
            return;
        }

        $config->setMetadataCacheImpl( new ApcCache() );
        $config->setQueryCacheImpl( new ApcCache() );
        // $config->setResultCacheImpl( new ApcCache() );
    }

    /**
     * Carrega tipos adicionais no Doctrine
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    private function loadTypes () {
        $types = $this->config->get( 'doctrine.types' );
        if ( !is_array( $types ) ) {
            return;
        }

        $db_platform = $this->entityManager->getConnection()->getDatabasePlatform();
        foreach ( $types as $type => $cls ) {
            Type::addType( $type, $cls );
            $db_platform->registerDoctrineTypeMapping( $type, $type );
        }
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager () {
        return $this->entityManager;
    }

}