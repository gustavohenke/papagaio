<?php

namespace Papagaio\Core;

use Papagaio\Entity\Visitor\EntityVisitor;
use Papagaio\Exception\AppException;
use Papagaio\Exception\UnknownErrorException;
use Papagaio\Utils\Utils;
use Slim\Slim;

class Response {

    /**
     * @var Slim
     */
    private $slim;

    /**
     * @var \Slim\Http\Response
     */
    private $response;

    function __construct ( Slim $slim, \Slim\Http\Response $response ) {
        $this->slim = $slim;
        $this->response = $response;
    }

    /**
     * Obtem/define o status HTTP
     *
     * @param   int|null $status
     * @return  int
     */
    public function status ( $status = null ) {
        return $this->response->status( $status );
    }

    /**
     * Obtem um header/define um header na resposta.
     *
     * @param   string $name
     * @param   mixed $value
     * @return  string
     */
    public function header ( $name, $value = null ) {
        return $this->response->header( $name, $value );
    }

    /**
     * Obtem/define o corpo da resposta.
     *
     * @param   string|null $body
     * @return  string
     */
    public function body ( $body = null ) {
        return $this->response->body( $body );
    }

    /**
     * @param   $content
     * @param   array $data
     * @param   EntityVisitor $visitor
     */
    public function render ( $content, array $data = [], EntityVisitor $visitor = null ) {
        if ( $content instanceof AppException ) {
            $this->status( $content->getCode() );

            $content = [
                'err' => $content->getErrorCode(),
                'message' => $content->getMessage()
            ];

            $this->header( 'Content-Type', 'application/json' );
            $this->body( Utils::toJSON( $content ) );
        } else if ( $content instanceof \Exception ) {
            $this->render( new UnknownErrorException( $content->getMessage() ) );
        } else if ( is_array( $content ) || is_object( $content ) ) {
            $this->header( 'Content-Type', 'application/json' );
            $this->body( Utils::toJSON( $content, $visitor ) );
        } else if ( $content != null ) {
            $this->header( 'Content-Type', 'text/html' );
            App::getInstance()->render( $content, $data );
        }
    }

    /**
     * Define um header padrão para o total de itens da resposta.
     *
     * @param   int $size
     * @return  string
     */
    public function count ( $size = 0 ) {
        return $this->header( 'X-Total-Count', $size );
    }

    /**
     * Define a data da última modificação do request atual
     *
     * @param   int $time
     */
    public function lastModified ( $time ) {
        $this->slim->lastModified( $time );
    }

}