<?php

namespace Papagaio\Core;

class File {

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $type;

    /**
     * @var int
     */
    private $size;

    /**
     * @var string
     */
    private $content;

    /**
     * @return string
     */
    public function getName () {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName ( $name ) {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType () {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType ( $type ) {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getSize () {
        return $this->size;
    }

    /**
     * @param int $size
     */
    public function setSize ( $size ) {
        $this->size = $size;
    }

    /**
     * @return string
     */
    public function getContent () {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent ( $content ) {
        $this->content = $content;
    }

}