<?php

namespace Papagaio\Core;

use Papagaio\Entity\User;
use Papagaio\Utils\Utils;

class Request {

    const SAFE_REGEX = '/GET|HEAD|OPTIONS/i';

    /**
     * @var \Slim\Http\Request
     */
    private $request;
    private $requestBody;

    /**
     * @var array
     */
    private $files = [];
    private $user;

    public function __construct ( \Slim\Http\Request $request ) {
        $this->request = $request;
        $this->setBody();
    }

    /**
     * Encontra o corpo da requisição e define ele
     * @param   mixed $body
     */
    public function setBody ( $body = null ) {
        if ( $this->isSafe() ) {
            return;
        } else if ( $body != null ) {
            $this->requestBody = (object) $body;
            return;
        }

        if ( $this->mediaType() === 'application/json' ) {
            $this->requestBody = json_decode( $this->request->getBody() );
            return;
        }

        $methods = [ "post", "put", "patch", "delete" ];
        foreach ( $methods as $method ) {
            $is = 'is' . ucfirst( $method );
            if ( $this->request->$is() ) {
                $this->requestBody = $this->request->$method();
                break;
            }
        }

        if ( Utils::isAssociative( $this->requestBody ) ) {
            $this->requestBody = (object) $this->requestBody;
        }
    }

    /**
     * Retorna o corpo original da requisição
     * @return string
     */
    public function originalBody () {
        return $this->request->getBody();
    }

    /**
     * Retorna a URL completa da requisição
     *
     * @return string
     */
    public function url () {
        return $this->request->getUrl();
    }

    /**
     * Retorna o path da requisição
     *
     * @return string
     */
    public function path () {
        return $this->request->getPath();
    }

    /**
     * Retorna o scheme (HTTP/HTTPS) desta requisição.
     *
     * @return string
     */
    public function scheme () {
        return $this->request->getScheme();
    }

    /**
     * Detecta se a requisição é segura (se não altera o servidor, segundo a convenção REST).
     *
     * @return  bool
     */
    public function isSafe () {
        $method = $this->method();
        return preg_match( self::SAFE_REGEX, $method ) === 1;
    }

    /**
     * Retorna o Media Type (valor incluso no header Content-Type, sem parâmetros charset, etc).
     *
     * @return  null|string
     */
    public function mediaType () {
        return $this->request->getMediaType();
    }

    /**
     * Retorna os parâmetros de Media Type.
     *
     * @return  array
     */
    public function mediaTypeParams () {
        return $this->request->getMediaTypeParams();
    }

    /**
     * Retorna o valor de um header
     *
     * @param   string $key
     * @param   mixed $default
     * @return  mixed
     */
    public function header ( $key, $default = null ) {
        return $this->request->headers( $key, $default );
    }

    /**
     * Retorna o método da requisição.
     *
     * @return  string
     */
    public function method () {
        return $this->request->getMethod();
    }

    /**
     * Retorna um valor da query string.
     *
     * @param   string $name
     * @param   mixed $default
     * @return  array|mixed|null
     */
    public function query ( $name, $default = null ) {
        return $this->request->get( $name, $default );
    }

    /**
     * Retorna o corpo da requisição.
     *
     * @param   string|null $key    O nome de uma propriedade do corpo que se deseja obter
     * @param   mixed $default      O valor padrão para $key caso não exista no corpo.
     * @return  array|mixed
     */
    public function body ( $key = null, $default = null ) {
        $body = $this->requestBody;
        if ( $key == null ) {
            return $body;
        }

        if ( is_object( $body ) ) {
            return isset( $body->$key ) ? $body->$key : $default;
        }

        return isset( $body[ $key ] ) ? $body[ $key ] : $default;
    }

    /**
     * Obtem ou define um arquivo enviado no request
     *
     * @param   string $name
     * @param   File $data=null
     * @return  File
     */
    public function file ( $name, File $data = null ) {
        if ( $data != null ) {
            $this->files[ $name ] = $data;
        }

        return isset( $this->files[ $name ] ) ? $this->files[ $name ] : null;
    }

    /**
     * Obtem ou define o usuário da requisição
     *
     * @param   User $user
     * @return  User
     */
    public function user ( User $user = null ) {
        if ( $user != null ) {
            $this->user = $user;
        }

        return $this->user;
    }

}