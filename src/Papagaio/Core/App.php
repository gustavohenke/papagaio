<?php

namespace Papagaio\Core;

use Papagaio\Controllers\Controller;
use Papagaio\Middleware\Middleware;
use Papagaio\Model\Model;
use Slim\Route;
use Slim\Slim;
use Slim\Views\Twig;
use Slim\Views\TwigExtension;

/**
 * Classe responsável pela inicialização do app.
 * É um singleton.
 *
 * @package Papagaio\Core
 */
class App {

    /**
     * Instância do singleton da aplicação.
     *
     * @var App
     */
    private static $instance;

    /**
     * @return  App
     */
    public static function getInstance () {
        if ( !isset( self::$instance ) ) {
            self::$instance = new App();
        }

        return self::$instance;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @var Slim
     */
    private $slim;

    /**
     * Diretório base do App
     * @var string
     */
    private $dir;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var Response
     */
    private $response;

    private function __construct () {
        $this->dir = getcwd();
    }

    /**
     * Inicializa a aplicação.
     * Carrega todas as rotas necessárias.
     */
    public function init () {
        $config = $this->config();

        // Configura timezone padrão
        date_default_timezone_set( $config->get( 'timezone' ) );

        // Obtem rotas e middlewares globais
        $routes = $config->get( 'routes' );
        $middlewares = $config->get( 'middlewares' );

        // Obtem configurações do Slim, e define a view como o Twig
        $slim = $config->get( 'slim' );
        $slim[ 'view' ] = new Twig();
        $slim[ 'log.writer' ] = new Logger();

        $this->slim = new Slim( $slim );

        // Carrega middlewares globais, desconsiderando os que forem inválidos
        $middlewares = $this->mapMiddlewares( $middlewares );
        array_map( [ $this->slim, 'add' ], $middlewares );

        // Carrega rotas
        foreach ( $routes as $route ) {
            $handler = $this->loadHandler( $route[ 'handler' ] );

            // Desconsidera handlers inválidos
            if ( $handler === null ) {
                continue;
            }

            // Cria um array de middlewares para a rota atual
            $middlewares = isset( $route[ 'middlewares' ] ) ? $route[ 'middlewares' ] : [];
            $middlewares = $this->mapMiddlewares( $middlewares );
            $middlewares = array_map( [ $this, 'wrapAsRouteMiddleware' ], $middlewares );

            $args = [ $route[ 'url' ] ];
            $args = array_merge( $args, $middlewares );
            array_push( $args, $handler );

            // Mapeia o request
            /** @var Route $route_obj */
            $route_obj = call_user_func_array( [ $this->slim, 'map' ], $args );
            $route_obj->via( $route[ 'method' ] );
            if ( $route[ 'method' ] === 'GET' ) {
                $route_obj->via( 'HEAD' );
            }

            isset( $route[ 'name' ] ) && $route_obj->name( $route[ 'name' ] );
        }

        // Configura handlers de erros/not found
        $error_handler = $this->loadHandler( $config->get( 'handlers.error' ) );
        $this->slim->error( $error_handler );
        $this->slim->notFound( $this->loadHandler( $config->get( 'handlers.notFound' ) ) );

        $this->configureView();

        // Se uma exception ocorrer em um dos middlewares da aplicação, não vai acontecer a chamada
        // ao error handler. Então, iremos utilizá-lo diretamente, e, como o Slim é muito bom,
        // temos que replicar o seu funcionamento interno de finalização do request...
        try {
            $this->slim->run();
        } catch ( \Exception $e ) {
            $error_handler( $e );
            list($status, $headers, $body) = $this->slim->response->finalize();

            $status_msg = sprintf(
                "HTTP/%s %s",
                $this->slim->config( 'http.version' ),
                \Slim\Http\Response::getMessageForCode( $status )
            );

            header( $status_msg );
            foreach ( $headers as $name => $value ) {
                header( "$name: $value" );
            }

            echo $body;
        }
    }

    /**
     * Configura a view
     */
    private function configureView () {
        $view = $this->slim->view();

        $view->parserOptions = array(
            'cache' => $this->config()->get( 'cache.views' )
        );

        $view->parserExtensions = array(
            new TwigExtension()
        );
    }

    /**
     * Retorna o diretório base da aplicação
     *
     * @return string
     */
    public function dir () {
        return $this->dir;
    }

    /**
     * Renderiza um arquivo de template.
     *
     * @param   string $template
     * @param   array $data
     */
    public function render ( $template, array $data = [] ) {
        $this->slim->render( $template, $data );
    }

    /**
     * Retorna o logger da aplicação
     *
     * @return  \Slim\Log
     */
    public function log () {
        return $this->slim->getLog();
    }

    /**
     * Retorna o request atual.
     *
     * @return  Request
     */
    public function request () {
        if ( $this->request == null ) {
            $this->request = new Request( $this->slim->request );
        }

        return $this->request;
    }

    /**
     * Retorna o response atual.
     *
     * @return Response
     */
    public function response () {
        if ( $this->response == null ) {
            $this->response = new Response( $this->slim, $this->slim->response );
        }

        return $this->response;
    }

    /**
     * Para a requisição atual.
     *
     * @throws \Slim\Exception\Stop
     */
    public function stop () {
        return $this->slim->stop();
    }

    /**
     * Constroi a URL para uma rota
     *
     * @param   $name
     * @param   array $params
     * @return  string
     */
    public function buildUrl ( $name, array $params = [] ) {
        return $this->slim->urlFor( $name, $params );
    }

    /**
     * Retorna a URL base do app
     *
     * @return string
     */
    public function base () {
        return $this->slim->request->getUrl();
    }

    /**
     * Retorna as configurações do App.
     *
     * @return  Config
     */
    public function config () {
        return Config::getInstance();
    }

    /**
     * Retorna um model.
     *
     * @param   string $name
     * @return  Model
     * @throws  \Exception  Caso o model desejado não exista.
     */
    public function model ( $name ) {
        return Model::createModel( $name );
    }

    /**
     * @param   string $handler
     * @return  callable|null
     */
    private function loadHandler ( $handler ) {
        $cls = '\Papagaio\Controllers\\' . $handler;

        // Ignora se não for uma implementação de Controller
        if ( !is_subclass_of( $cls, '\Papagaio\Controllers\Controller' ) ) {
            return null;
        }

        return function () use ( $cls ) {
            /** @var Controller $ctrl */
            $ctrl = new $cls();
            $ctrl->execute( $this, $this->request(), $this->response(), func_get_args() );
        };
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Instancia uma classe middleware
     *
     * @param   string $name    Nome da classe middleware para carregar
     * @return  Middleware
     */
    private function loadMiddleware ( $name ) {
        $name = ucfirst( $name ) . 'Middleware';
        $cls = '\Papagaio\Middleware\\' . $name;

        // Ignore se não for uma implementação de Middleware
        if ( !is_subclass_of( $cls, '\Papagaio\Middleware\Middleware' ) ) {
            return null;
        }

        return new $cls();
    }

    /**
     * Mapeia nomes de middlewares para instâncias dos mesmos.
     *
     * @param   array $middlewares
     * @return  array
     */
    private function mapMiddlewares ( array $middlewares ) {
        $middlewares = array_map( [ $this, 'loadMiddleware' ], $middlewares );
        return array_filter( $middlewares, 'is_object' );
    }

    /**
     * Envolve um middleware no formato global para um middleware de rota do Slim
     *
     * @param   Middleware $middleware
     * @return  \Closure
     */
    private function wrapAsRouteMiddleware ( Middleware $middleware ) {
        $middleware->setNextMiddleware( new DummyMiddleware() );

        return function () use ( $middleware ) {
            $middleware->call();
        };
    }
}

class DummyMiddleware extends \Slim\Middleware {
    public function call () {}
}