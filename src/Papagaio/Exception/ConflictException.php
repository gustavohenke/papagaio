<?php

namespace Papagaio\Exception;

use Papagaio\Utils\HttpStatus;

class ConflictException extends AppException {

    public function __construct ( $message ) {
        parent::__construct( $message, HttpStatus::HTTP_CONFLICT );
        $this->errorCode = "CONFLICT";
    }

}