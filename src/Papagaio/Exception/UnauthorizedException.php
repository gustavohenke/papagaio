<?php

namespace Papagaio\Exception;

use Papagaio\Utils\HttpStatus;

class UnauthorizedException extends AppException {

    public function __construct( $message = "Autenticação requerida." ) {
        parent::__construct( $message, HttpStatus::HTTP_UNAUTHORIZED );
        $this->errorCode = "UNAUTHORIZED";
    }

}