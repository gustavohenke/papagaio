<?php

namespace Papagaio\Exception;

use Papagaio\Utils\HttpStatus;

class UnknownErrorException extends AppException {

    public function __construct ( $message ) {
        parent::__construct( $message, HttpStatus::HTTP_INTERNAL_SERVER_ERROR );
        $this->errorCode = "UNKNOWN_ERROR";
    }

}