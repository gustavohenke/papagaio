<?php

namespace Papagaio\Exception;

use Papagaio\Utils\HttpStatus;

class RequestTooLargeException extends AppException {

    public function __construct ( $message ) {
        parent::__construct( $message, HttpStatus::HTTP_REQUEST_ENTITY_TOO_LARGE );
        $this->errorCode = "REQUEST_TOO_LARGE";
    }
}