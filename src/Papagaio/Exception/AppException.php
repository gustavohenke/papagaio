<?php

namespace Papagaio\Exception;

class AppException extends \Exception {

    /**
     * @var string
     */
    protected $errorCode;

    /**
     * @return string
     */
    public function getErrorCode () {
        return $this->errorCode;
    }

}