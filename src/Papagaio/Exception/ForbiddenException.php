<?php

namespace Papagaio\Exception;

use Papagaio\Utils\HttpStatus;

class ForbiddenException extends AppException {

    public function __construct ( $message ) {
        parent::__construct( $message, HttpStatus::HTTP_FORBIDDEN );
        $this->errorCode = "FORBIDDEN";
    }

}