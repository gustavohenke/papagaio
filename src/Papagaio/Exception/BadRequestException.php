<?php

namespace Papagaio\Exception;

use Papagaio\Utils\HttpStatus;

class BadRequestException extends AppException {

    public function __construct ( $message ) {
        parent::__construct( $message, HttpStatus::HTTP_BAD_REQUEST );
        $this->errorCode = "BAD_REQUEST";
    }

}