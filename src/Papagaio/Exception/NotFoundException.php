<?php

namespace Papagaio\Exception;

use Papagaio\Utils\HttpStatus;

class NotFoundException extends AppException {

    public function __construct ( $message ) {
        parent::__construct( $message, HttpStatus::HTTP_NOT_FOUND );
        $this->errorCode = 'NOT_FOUND';
    }

}