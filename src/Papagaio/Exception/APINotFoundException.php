<?php

namespace Papagaio\Exception;

use Papagaio\Utils\HttpStatus;

class APINotFoundException extends AppException {

    public function __construct () {
        parent::__construct( "API não encontrada", HttpStatus::HTTP_NOT_FOUND );
        $this->errorCode = "API_NOT_FOUND";
    }

}