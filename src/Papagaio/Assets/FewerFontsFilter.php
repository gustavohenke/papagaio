<?php

namespace Papagaio\Assets;

use Assetic\Asset\AssetInterface;
use Assetic\Filter\FilterInterface;

class FewerFontsFilter implements FilterInterface {

    private static $extensions = [
        'eot',
        'svg',
        'ttf'
    ];

    private function testExtension ( $url ) {
        foreach ( self::$extensions as $ext ) {
            if ( stripos( $url, '.' . $ext ) !== false ){
                return true;
            }
        }

        return false;
    }

    /**
     * Filters an asset after it has been loaded.
     *
     * @param AssetInterface $asset An asset
     */
    public function filterLoad ( AssetInterface $asset ) {
        $matches = [];
        $css = $asset->getContent();
        preg_match_all( '/url\((.+?)\)(\s+format\(.+?\))?,?/', $css, $matches );

        foreach( $matches[ 0 ] as $url ) {
            if ( !$this->testExtension( $url ) ) {
                continue;
            }

            $css = preg_replace( '#' . preg_quote( $url, '#' ) . '#', '', $css, 1 );
        }

        $css = preg_replace( '#src:\s*;#', '', $css );
        $css = preg_replace( '#(src:.+?),\s*;#', '$1;', $css );
        $asset->setContent( $css );
    }

    /**
     * Filters an asset just before it's dumped.
     *
     * @param AssetInterface $asset An asset
     */
    public function filterDump ( AssetInterface $asset ) {}
}