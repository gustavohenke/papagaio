<?php

namespace Papagaio\Assets;

use Assetic\Asset\AssetInterface;
use Assetic\Filter\FilterInterface;

class CssEmbedFilter implements FilterInterface {

    private $info;

    public function __construct () {
        $this->info = new \finfo( FILEINFO_MIME_TYPE );
    }

    /**
     * Filters an asset after it has been loaded.
     *
     * @param AssetInterface $asset An asset
     */
    public function filterLoad ( AssetInterface $asset ) {
        $matches = [];
        $root = $asset->getSourceDirectory();
        $css = $asset->getContent();
        preg_match_all( '/url\((.+?)\)/', $css, $matches );

        foreach( $matches[ 1 ] as $url ) {
            $url = trim( $url, "\"'" );
            if ( stripos( $url, "http" ) === 0 || strpos( $url, "data:" ) === 0 ) {
                continue;
            }

            try {
                $url_path = preg_replace( '/\?.*$/', '', $url );
                $url_path = realpath( $root . '/' . $url_path );
                $mime = $this->info->file( $url_path );
                $contents = base64_encode( file_get_contents( $url_path ) );

                $css = preg_replace(
                    '#' . preg_quote( $url, '#' ) . '#',
                    'data:' . $mime . ';base64,' . $contents,
                    $css,
                    1
                );
            } catch ( \Exception $e ) {}
        }
        $asset->setContent( $css );
    }

    /**
     * Filters an asset just before it's dumped.
     *
     * @param AssetInterface $asset An asset
     */
    public function filterDump ( AssetInterface $asset ) {
        // TODO: Implement filterDump() method.
    }
}