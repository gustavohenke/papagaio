<?php

namespace Papagaio\Assets;

use Assetic\Asset\AssetInterface;
use Assetic\Filter\FilterInterface;

class JSWrapperFilter implements FilterInterface {

    /**
     * Filters an asset after it has been loaded.
     *
     * @param AssetInterface $asset An asset
     */
    public function filterLoad ( AssetInterface $asset ) {
        $js = $asset->getContent();

        // Separa o JS por linhas, e adiciona 4 espaços no inicio de cada linha.
        // Isto é para que fique mais legível no arquivo de saída depois.
        $js = preg_split( '/\R/u', $js );
        foreach ( $js as &$line ) {
            $line = "    " . $line;
        }
        $js = implode( "\n", $js );

        $js = <<<JS
!function () {
$js
}();

JS;

        $asset->setContent( $js );
    }

    /**
     * Filters an asset just before it's dumped.
     *
     * @param AssetInterface $asset An asset
     */
    public function filterDump ( AssetInterface $asset ) {}
}