<?php

namespace Papagaio\Assets;

use Assetic\Asset\AssetInterface;
use Assetic\Filter\FilterInterface;

class AngularFilter implements FilterInterface {

    private $module;

    public function __construct ( $module ) {
        $this->module = $module;
    }

    /**
     * Filters an asset after it has been loaded.
     *
     * @param AssetInterface $asset An asset
     */
    public function filterLoad ( AssetInterface $asset ) {
        $module = $this->module;
        $name = $asset->getSourcePath();
        $html = addslashes( $asset->getContent() );
        $html = preg_split( '/\R/u', $html );
        foreach ( $html as &$line ) {
            $line = sprintf( '"%s" +', $line );
        }

        $html = implode( "\n", $html );
        $html = rtrim( $html, '+' );

        $js = <<<JS
angular.module( "$module" ).run([ "\$templateCache", function ( \$templateCache ) {
    \$templateCache.put( "$name", $html );
}]);
JS;

        $asset->setContent( $js );
    }

    /**
     * Filters an asset just before it's dumped.
     *
     * @param AssetInterface $asset An asset
     */
    public function filterDump ( AssetInterface $asset ) {}
}