<?php

namespace Papagaio\Controllers\ErrorHandler;


use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;

class GeneralErrorHandler implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app A aplicação
     * @param   Request $request A requisição atual
     * @param   Response $response A resposta atual
     * @param   array $args Argumentos recebidos na URL
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        /** @var \Exception $error */
        $error = $args[ 0 ];

        // Na esperança de fazer log no Heroku...
        $app->log()->error( $error );

        if ( strpos( $request->path(), '/api' ) === 0 ) {
            $response->render( $error );
        } else {
            $response->render( 'views/app.html' );
        }

        // É necessário fazer isso ou o Slim vai ferrar com o output deste handler
        // $app->stop();
    }


}