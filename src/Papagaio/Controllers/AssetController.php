<?php

namespace Papagaio\Controllers;

use Assetic\Asset\AssetCache;
use Assetic\Asset\AssetCollection;
use Assetic\Asset\AssetInterface;
use Assetic\Asset\GlobAsset;
use Assetic\Cache\FilesystemCache;
use Assetic\Filter\LessphpFilter;
use Papagaio\Assets\AngularFilter;
use Papagaio\Assets\CssEmbedFilter;
use Papagaio\Assets\FewerFontsFilter;
use Papagaio\Assets\JSWrapperFilter;
use Papagaio\Core\App;
use Papagaio\Core\Config;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Utils\HttpStatus;

class AssetController implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app            A aplicação
     * @param   Request $request    A requisição atual
     * @param   Response $response  A resposta atual
     * @param   array $args         Argumentos recebidos na URL
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        $config = $app->config();
        $assets = $config->get( 'assetic.assets' );
        $asset = isset( $assets[ $args[ 0 ] ] ) ? $assets[ $args[ 0 ] ] : null;

        // Asset não encontrado?
        if ( $asset === null ) {
            // Seta a resposta como 404 e retorna.
            $response->status( 404 );
            $response->header( 'Content-Type', $this->getContentType() );
            return;
        }

        // Converte cada arquivo do asset para um glob
        foreach ( $asset[ 'files' ] as &$file ) {
            $file = $this->convertPattern( $file, $config );
        }

        // Cria um AssetCollection e já faz cache dele
        $asset_process = new AssetCollection( $asset[ 'files' ] );
        $asset_process = $this->cache( $asset_process );

        // Carrega o conteúdo do asset, preparando-o como for necessário
        $asset_process->load();

        // Seta a data da última modificação do asset
        $response->lastModified( $asset_process->getLastModified() );

        // Seta o content-type correto para o tipo de asset
        $response->header( 'Content-Type', $this->getContentType( $asset ) );

        // Responde com o corpo do asset
        $response->body( $asset_process->getContent() );
    }

    /**
     * Talvez aplica uma camada de cache sobre um asset.
     *
     * @param   $asset_process
     * @return  AssetInterface
     */
    private function cache ( AssetInterface $asset_process ) {
        $config = App::getInstance()->config();
        $cache = $config->get( 'cache.assets' );
        if ( !$cache ) {
            return $asset_process;
        }

        return new AssetCache(
            $asset_process,
            new FilesystemCache( $config->get( 'cache.assets' ) )
        );
    }

    /**
     * Converte uma mapeamento para um asset do Assetic.
     *
     * @param   string $file    Caminho/glob do arquivo
     * @param   Config $config  Configurações do projeto
     * @return  GlobAsset
     */
    private function convertPattern ( $file, Config $config ) {
        $filters = [];
        $ext = array_pop( preg_split( '#\.#', $file ) );
        $ext = strtolower( $ext );

        switch ( $ext ) {
            case 'css':
                $remove_eot = new FewerFontsFilter();
                $filters[] = $remove_eot;

                $embed = new CssEmbedFilter();
                $filters[] = $embed;
                break;

            case 'less':
                $less = new LessphpFilter();
                $less->setPreserveComments( true );
                array_push( $filters, $less );
                break;

            case 'html':
                $ng = new AngularFilter( "app" );
                array_push( $filters, $ng );
                break;

            case 'js':
                $wrapper = new JSWrapperFilter();
                array_push( $filters, $wrapper );
                break;

            default:
                break;
        }

        $root = $config->get( 'assetic.root' );
        return new GlobAsset( $root . '/' . $file, $filters, $root );
    }

    /**
     * Retorna o Content-Type para uma configuração de asset.
     *
     * @param   array $asset    A configuração do asset
     * @return  string          O Content-Type mapeado para o tipo do asset.
     */
    private function getContentType ( $asset = array() ) {
        $type = isset( $asset[ 'type' ] ) ? $asset[ 'type' ] : null;
        switch ( $type ) {
            case 'js':
                return 'text/javascript';

            case 'css':
                return 'text/css';

            case 'svg':
                return 'image/svg+xml';

            case 'png':
                return 'image/png';

            default:
                return 'text/plain';
        }
    }
}