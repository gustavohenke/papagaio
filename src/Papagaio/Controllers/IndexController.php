<?php

namespace Papagaio\Controllers;

use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;

class IndexController implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app A aplicação
     * @param   Request $request A requisição atual
     * @param   Response $response A resposta atual
     * @param   array $args Argumentos recebidos na URL
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        $response->render( 'views/app.html' );
    }
}