<?php

namespace Papagaio\Controllers;

use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;

class IllustrationController implements Controller {

    /**
     * @var \finfo
     */
    private $info;

    /**
     * Inicializa o controller
     */
    public function __construct () {
        $this->info = new \finfo( FILEINFO_MIME );
    }

    /**
     * Executa um request.
     *
     * @param   App $app A aplicação
     * @param   Request $request A requisição atual
     * @param   Response $response A resposta atual
     * @param   array $args Argumentos recebidos na URL
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        $root = $app->config()->get( 'assetic.illustrations' );
        $illustration = $args[ 0 ];
        $file = $root . '/' . $illustration;

        // Se o arquivo não existe, já para por aqui.
        if ( !file_exists( $file ) ) {
            $response->status( 404 );
            $response->render( null );
            return;
        }

        $response->lastModified( filemtime( $file ) );
        $response->header( 'Content-Type', $this->info->file( $file ) );
        readfile( $file );
    }
}