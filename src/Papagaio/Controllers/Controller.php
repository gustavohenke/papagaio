<?php

namespace Papagaio\Controllers;

use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;

/**
 * Interface para utilização de controllers.
 * Esta interface usa o Design Pattern Command.
 *
 * @package Papagaio\Controller
 */
interface Controller {

    /**
     * Executa um request.
     *
     * @param   App $app A aplicação
     * @param   Request $request A requisição atual
     * @param   Response $response A resposta atual
     * @param   array $args Argumentos recebidos na URL
     * @return
     */
    public function execute ( App $app, Request $request, Response $response, array $args );

}