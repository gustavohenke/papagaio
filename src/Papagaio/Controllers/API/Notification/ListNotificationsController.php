<?php

namespace Papagaio\Controllers\API\Notification;

use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Model\NotificationModel;

class ListNotificationsController implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app            A aplicação
     * @param   Request $request    A requisição atual
     * @param   Response $response  A resposta atual
     * @param   array $args         Argumentos recebidos na URL
     * @return  void
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        /** @var NotificationModel $notificationModel */
        $notificationModel = $app->model( 'notification' );
        $notifications = $notificationModel->find( $request->user() );

        $response->count( $notifications->count() );
        $response->render( $notifications );
    }
}