<?php

namespace Papagaio\Controllers\API\Like;

use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Model\LikeModel;
use Papagaio\Model\PostModel;

class GetLikeController implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app A aplicação
     * @param   Request $request A requisição atual
     * @param   Response $response A resposta atual
     * @param   array $args Argumentos recebidos na URL
     * @return  void
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        /** @var LikeModel $likeModel */
        $likeModel = $app->model( 'like' );

        /** @var PostModel $postModel */
        $postModel = $app->model( 'post' );
        $post = $postModel->find( $args[ 0 ] );

        $like = $likeModel->find( $post, $request->user() );
        $response->body( $like );
    }

}