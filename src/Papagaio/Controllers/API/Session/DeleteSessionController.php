<?php

namespace Papagaio\Controllers\API\Session;

use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Model\UserModel;
use Papagaio\Utils\HttpStatus;

class DeleteSessionController implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app A aplicação
     * @param   Request $request A requisição atual
     * @param   Response $response A resposta atual
     * @param   array $args Argumentos recebidos na URL
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        /** @var UserModel $user_model */
        $user_model = $app->model( 'user' );

        $session_id = $args[ 0 ];
        $user_model->logout( $request->user(), $session_id );

        $response->status( HttpStatus::HTTP_NO_CONTENT );
        $response->render( null );
    }
}