<?php

namespace Papagaio\Controllers\API\Session;

use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Model\UserModel;

class GetSessionController implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app A aplicação
     * @param   Request $request A requisição atual
     * @param   Response $response A resposta atual
     * @param   array $args Argumentos recebidos na URL
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        /** @var UserModel $user_model */
        $user_model = $app->model( 'user' );

        $user = $request->user();
        $session_id = $args[ 0 ];
        $session = $user_model->findSession( $user, $session_id );

        // Garante que esta resposta não seja cacheada nunca
        $response->header( 'Cache-Control', 'private' );

        $visitor = new OwnSessionVisitor( $user );
        $response->render( $session, [], $visitor );
    }
}