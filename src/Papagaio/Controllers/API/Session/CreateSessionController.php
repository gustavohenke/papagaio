<?php

namespace Papagaio\Controllers\API\Session;

use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Entity\User;
use Papagaio\Exception\BadRequestException;
use Papagaio\Exception\UnauthorizedException;
use Papagaio\Model\UserModel;
use Papagaio\Utils\HttpStatus;

class CreateSessionController implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app A aplicação
     * @param   Request $request A requisição atual
     * @param   Response $response A resposta atual
     * @param   array $args Argumentos recebidos na URL
     * @throws  BadRequestException
     * @throws  \Exception
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        /** @var UserModel $user_model */
        $user_model = $app->model( 'user' );

        $data = $request->body();
        if ( !is_object( $data ) ) {
            throw new BadRequestException( "O corpo deveria ser um objeto" );
        }

        $user = new User();
        $user->setUsername( isset( $data->username ) ? $data->username : null );
        $user->setPassword( isset( $data->password ) ? $data->password : null );
        if ( !( $result = $user_model->login( $user ) ) ) {
            throw new UnauthorizedException( "Usuário e/ou senha inválidos!" );
        }

        $response->status( HttpStatus::HTTP_CREATED );
        $response->render( $result );
    }
}