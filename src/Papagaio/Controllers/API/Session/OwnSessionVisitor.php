<?php

namespace Papagaio\Controllers\API\Session;

use Papagaio\Entity\User;
use Papagaio\Entity\Visitor\DefaultVisitor;
use Papagaio\Entity\Visitor\EntityVisitor;

class OwnSessionVisitor extends DefaultVisitor implements EntityVisitor {

    /**
     * Usuário dono da sessão sendo visitada.
     *
     * @var User
     */
    private $user;

    public function __construct ( User $user ) {
        $this->user = $user;
    }

    /**
     * @param   User $user
     * @return  mixed|void
     */
    public function visitUser ( User $user ) {
        $data = parent::visitUser( $user );

        if ( $user != $this->user ) {
            return;
        }

        $data[ 'email' ] = $user->getEmail();
        return $data;
    }

}