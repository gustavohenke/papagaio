<?php

namespace Papagaio\Controllers\API\HashTag;

use Papagaio\Entity\HashTag;
use Papagaio\Entity\Visitor\DefaultVisitor;
use Papagaio\Entity\Visitor\EntityVisitor;
use Papagaio\Model\HashTagModel;

class HashTagVisitor extends DefaultVisitor implements EntityVisitor {

    private $model;

    public function __construct ( HashTagModel $model ) {
        $this->model = $model;
    }

    public function visitHashTag ( HashTag $hashTag ) {
        return [
            'id'            => $hashTag->getId(),
            'createdAt'     => $hashTag->getCreatedAt(),
            'postsCount'    => $this->model->countPosts( $hashTag ),
            'postsUrl'      => $hashTag->getPostsUrl(),
            'url'           => $hashTag->getUrl()
        ];
    }


}