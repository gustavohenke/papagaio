<?php

namespace Papagaio\Controllers\API\HashTag;

use Doctrine\Common\Collections\ArrayCollection;
use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Model\HashTagModel;

class ListHashTagsController implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app A aplicação
     * @param   Request $request A requisição atual
     * @param   Response $response A resposta atual
     * @param   array $args Argumentos recebidos na URL
     * @return  void
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        /** @var HashTagModel $hashtag_model */
        $hashtag_model = $app->model( 'hashTag' );

        /** @var Collection $hashtags */
        $hashtags = null;

        $search = $request->query( 'search' );
        if ( empty( $search ) ) {
            $hashtags = $hashtag_model->findAll();
        } else {
            $hashtags = $hashtag_model->findAllById( $search );
        }

        $response->count( $hashtags->count() );
        $response->render( $hashtags, null, new HashTagVisitor( $hashtag_model ) );
    }
}