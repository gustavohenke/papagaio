<?php

namespace Papagaio\Controllers\API\User\Photo;

use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Entity\User;
use Papagaio\Model\ImageModel;
use Papagaio\Model\UserModel;

class GetUserCoverController implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app A aplicação
     * @param   Request $request A requisição atual
     * @param   Response $response A resposta atual
     * @param   array $args Argumentos recebidos na URL
     * @return  void
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        /** @var UserModel $user_model */
        $user_model = $app->model( 'user' );

        /** @var ImageModel $image_model */
        $image_model = $app->model( 'image' );

        $user = $user_model->find( $args[ 0 ] );
        $photo = $image_model->findUserPhoto( $user->getPhoto() );

        $filename = $this->getFilename( $user );
        $response->header( 'Content-Type', ImageModel::MIMETYPE );
        $response->header( 'Content-Disposition', 'inline; filename=' . $filename );

        $width = $height = null;
        if ( isset( $args[ 1 ] ) ) {
            list( $width, $height ) = explode( 'x', $args[ 1 ] );
        }

        $content = $image_model->process(
            $photo->getContent(),
            intval( $width ),
            intval( $height )
        );
        $content = $image_model->blur( $content );

        $response->body( $content );
    }

    private function getFilename ( User $user ) {
        return 'cover_' . $user->getId() . '.' . ImageModel::EXTENSION;
    }
}