<?php

namespace Papagaio\Controllers\API\User\Logged;

use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Exception\NotFoundException;
use Papagaio\Model\FollowModel;
use Papagaio\Model\UserModel;
use Papagaio\Utils\HttpStatus;

class GetLoggedUserFollowingController implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app            A aplicação
     * @param   Request $request    A requisição atual
     * @param   Response $response  A resposta atual
     * @param   array $args         Argumentos recebidos na URL
     * @return  void
     * @throws  NotFoundException   Se o usuário logado não seguir o usuário testado
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        /** @var FollowModel $followModel */
        $followModel = $app->model( 'follow' );

        /** @var UserModel $userModel */
        $userModel = $app->model( 'user' );
        $user = $userModel->find( $args[ 0 ] );

        // Caso a relação de seguidor não exista, então vamos simplesmente deixar a exception rolar.
        $followModel->find( $user, $request->user() );
        $response->status( HttpStatus::HTTP_NO_CONTENT );
    }
}