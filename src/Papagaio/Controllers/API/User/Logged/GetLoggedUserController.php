<?php

namespace Papagaio\Controllers\API\User\Logged;

use Papagaio\Controllers\API\User\GetUserController;
use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;

class GetLoggedUserController extends GetUserController implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app A aplicação
     * @param   Request $request A requisição atual
     * @param   Response $response A resposta atual
     * @param   array $args Argumentos recebidos na URL
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        $logged_user = $request->user();
        $response->render( $logged_user, null, new LoggedUserVisitor() );
    }
}