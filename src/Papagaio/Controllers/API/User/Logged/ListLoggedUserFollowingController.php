<?php

namespace Papagaio\Controllers\API\User\Logged;

use Papagaio\Controllers\API\User\Follower\ListFollowingController;
use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;

class ListLoggedUserFollowingController extends ListFollowingController implements Controller {

    public function execute ( App $app, Request $request, Response $response, array $args ) {
        parent::execute( $app, $request, $response, [
            $request->user()->getUsername()
        ]);
    }

}