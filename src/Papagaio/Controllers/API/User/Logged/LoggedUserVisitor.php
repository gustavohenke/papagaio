<?php

namespace Papagaio\Controllers\API\User\Logged;

use Papagaio\Entity\User;
use Papagaio\Entity\Visitor\DefaultVisitor;

class LoggedUserVisitor extends DefaultVisitor {

    public function visitUser ( User $user ) {
        $data = parent::visitUser( $user );
        $data[ 'email' ] = $user->getEmail();

        return $data;
    }


}