<?php

namespace Papagaio\Controllers\API\User\Logged;

use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Entity\User;
use Papagaio\Entity\UserPhoto;
use Papagaio\Exception\BadRequestException;
use Papagaio\Exception\ForbiddenException;
use Papagaio\Exception\NotFoundException;
use Papagaio\Exception\ValidationException;
use Papagaio\Model\UserModel;

class UpdateLoggedUserController implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app            A aplicação
     * @param   Request $request    A requisição atual
     * @param   Response $response  A resposta atual
     * @param   array $args         Argumentos recebidos na URL
     * @throws  BadRequestException Quando o corpo da requisição não é um objeto
     * @throws  ValidationException Quando alguma das propriedades obrigatórias não foi informada
     * @throws  NotFoundException   Quando o usuário não existir
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        /** @var UserModel $user_model */
        $user_model = $app->model( 'user' );

        $body = $request->body();
        if ( !is_object( $body ) ) {
            throw new BadRequestException( "O corpo deveria ser um objeto" );
        } else if ( !isset( $body->username ) ) {
            throw new ValidationException( "O nome do usuário deve ser fornecido." );
        } else if ( !isset( $body->email ) ) {
            throw new ValidationException( "O e-mail deve ser fornecido." );
        }

        $user = new User();
        $user->setUsername( $body->username );
        $user->setEmail( $body->email );

        // Como a senha é opcional, então vamos defini-la apenas se tiver sido passada no request
        if ( isset( $body->password ) ) {
            $user->setPassword( $body->password );
        }

        // Se uma foto foi enviada neste request, devemos passar o conteúdo dela e o tipo
        $photo_file = $request->file( 'photo' );
        if ( $photo_file != null ) {
            $photo = new UserPhoto();
            $photo->setType( $photo_file->getType() );
            $photo->setContent( $photo_file->getContent() );

            $user->setPhoto( $photo );
        }

        $user = $user_model->update( $request->user()->getUsername(), $user );
        $response->render( $user, null, new LoggedUserVisitor() );
    }
}