<?php

namespace Papagaio\Controllers\API\User;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Model\UserModel;

class ListUsersController implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app A aplicação
     * @param   Request $request A requisição atual
     * @param   Response $response A resposta atual
     * @param   array $args Argumentos recebidos na URL
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        /** @var UserModel $user_model */
        $user_model = $app->model( 'user' );

        /** @var Collection $results */
        $results = null;

        $search_term = $request->query( 'search' );
        if ( empty( $search_term ) ) {
            $results = $user_model->findAll();
        } else {
            $results = $user_model->findAllByUsername( $search_term );
        }

        $response->count( $results->count() );
        $response->render( $results );
    }


}