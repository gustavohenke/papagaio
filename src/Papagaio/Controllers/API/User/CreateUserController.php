<?php

namespace Papagaio\Controllers\API\User;

use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Entity\User;
use Papagaio\Exception\BadRequestException;
use Papagaio\Model\UserModel;
use Papagaio\Utils\HttpStatus;

class CreateUserController implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app A aplicação
     * @param   Request $request A requisição atual
     * @param   Response $response A resposta atual
     * @param   array $args Argumentos recebidos na URL
     * @throws  BadRequestException
     * @throws  \Papagaio\Exception\ConflictException
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        /** @var UserModel $user_model */
        $user_model = $app->model( 'user' );

        $body = $request->body();
        if ( !is_object( $body ) ) {
             throw new BadRequestException( "O corpo deveria ser um objeto" );
        }

        $user = new User();
        $user->setUsername( $body->username );
        $user->setPassword( $body->password );
        $user->setEmail( $body->email );

        $user_model->create( $user );
        $response->status( HttpStatus::HTTP_CREATED );
        $response->render( $user );
    }
}