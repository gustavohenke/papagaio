<?php

namespace Papagaio\Controllers\API\User;

use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Model\UserModel;

class GetUserController implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app A aplicação
     * @param   Request $request A requisição atual
     * @param   Response $response A resposta atual
     * @param   array $args Argumentos recebidos na URL
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        /** @var UserModel $user_model */
        $user_model = $app->model( 'user' );

        // O argumento vindo pela URL pode ser o id ou o login do usuário
        $user = $user_model->find( $args[ 0 ] );

        $response->status( 200 );
        $response->render( $user );
    }
}