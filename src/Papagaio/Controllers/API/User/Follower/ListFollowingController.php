<?php

namespace Papagaio\Controllers\API\User\Follower;

use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Model\FollowModel;
use Papagaio\Model\UserModel;

class ListFollowingController implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app A aplicação
     * @param   Request $request A requisição atual
     * @param   Response $response A resposta atual
     * @param   array $args Argumentos recebidos na URL
     * @return  void
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        /** @var UserModel $user_model */
        $user_model = $app->model( 'user' );

        /** @var FollowModel $followModel */
        $followModel = $app->model( 'follow' );

        $user = $user_model->find( $args[ 0 ] );
        $following = $followModel->mapFollowingToUsers( $user );

        $response->count( $following );
        $response->render( $following );
    }
}