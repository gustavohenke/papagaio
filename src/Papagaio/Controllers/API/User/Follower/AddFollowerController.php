<?php

namespace Papagaio\Controllers\API\User\Follower;

use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Exception\BadRequestException;
use Papagaio\Exception\ValidationException;
use Papagaio\Model\FollowModel;
use Papagaio\Model\UserModel;
use Papagaio\Utils\HttpStatus;

class AddFollowerController implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app A aplicação
     * @param   Request $request A requisição atual
     * @param   Response $response A resposta atual
     * @param   array $args Argumentos recebidos na URL
     * @throws  BadRequestException
     * @throws  ValidationException
     * @throws  \Papagaio\Exception\NotFoundException
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        /** @var UserModel $userModel */
        $userModel = $app->model( 'user' );

        /** @var FollowModel $followModel */
        $followModel = $app->model( 'follow' );

        $body = $request->body();
        if ( !is_object( $body ) ) {
            throw new BadRequestException( 'O corpo deveria ser um objeto' );
        } else if ( empty( $body->user ) ) {
            throw new ValidationException( 'O usuário para seguir deve ser fornecido' );
        }

        $followed = $userModel->find( $body->user );
        $followModel->follow( $request->user(), $followed );

        $response->status( HttpStatus::HTTP_NO_CONTENT );
    }
}