<?php

namespace Papagaio\Controllers\API\Post\Photo;

use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Entity\Post;
use Papagaio\Entity\PostPhoto;
use Papagaio\Exception\NotFoundException;
use Papagaio\Model\PostModel;

class GetPostPhotoController implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app            A aplicação
     * @param   Request $request    A requisição atual
     * @param   Response $response  A resposta atual
     * @param   array $args         Argumentos recebidos na URL
     * @throws  NotFoundException   Quando a foto não for encontrada
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        $post_id = $args[ 0 ];
        $photo_id = $args[ 1 ];

        /** @var PostModel $post_model */
        $post_model = $app->model( 'post' );
        $post = $post_model->find( $post_id );
        $photos = $post->getPhotos();

        /** @var PostPhoto $photo */
        foreach ( $photos as $photo ) {
            if ( $photo->getId() == $photo_id ) {
                $filename = $this->getFilename( $post, $photo );
                $response->header( 'Content-Disposition', 'inline; filename=' . $filename );
                $response->header( 'Content-Type', $photo->getType() );
                $response->lastModified( $post->getCreatedAt()->getTimestamp() );
                $response->body( stream_get_contents( $photo->getData() ) );
                return;
            }
        }

        throw new NotFoundException( "Foto não encontrada" );
    }

    private function getFilename ( Post $post, PostPhoto $photo ) {
        $mime = preg_split( '#/#', $photo->getType(), 2 );

        return 'photo_' . $post->getId() . '_' . $photo->getId() . '.' . $mime[ 1 ];
    }
}