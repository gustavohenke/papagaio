<?php

namespace Papagaio\Controllers\API\Post;

use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Model\HashTagModel;
use Papagaio\Model\PostModel;

class ListHashTagPostsController extends AbstractPostController implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app A aplicação
     * @param   Request $request A requisição atual
     * @param   Response $response A resposta atual
     * @param   array $args Argumentos recebidos na URL
     * @return  void
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        /** @var HashTagModel $hashtag_model */
        $hashtag_model = $app->model( 'hashTag' );

        /** @var PostModel $post_model */
        $post_model = $app->model( 'post' );

        $tag = $hashtag_model->find( $args[ 0 ] );
        $posts = $post_model->findAllByHashTag( $tag );

        $response->count( $posts->count() );
        $this->render( $posts );
    }
}