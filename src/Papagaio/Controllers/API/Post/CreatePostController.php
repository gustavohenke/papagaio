<?php

namespace Papagaio\Controllers\API\Post;

use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Entity\Post;
use Papagaio\Entity\PostPhoto;
use Papagaio\Exception\BadRequestException;
use Papagaio\Model\PostModel;
use Papagaio\Utils\HttpStatus;
use Papagaio\Utils\Utils;

class CreatePostController extends AbstractPostController implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app A aplicação
     * @param   Request $request A requisição atual
     * @param   Response $response A resposta atual
     * @param   array $args Argumentos recebidos na URL
     * @throws  BadRequestException
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        $post = $this->buildPost( $request );
        $this->createPost( $post, $response );
    }

    /**
     * Constroi o objeto inicial do post a ser criado.
     *
     * @param   Request $request    A requisição.
     * @return  Post                O post construído.
     * @throws  BadRequestException Se o corpo da requisição não tiver os dados necessários.
     */
    protected final function buildPost ( Request $request ) {
        $body = $request->body();
        if ( !is_object( $body ) ) {
            throw new BadRequestException( "O corpo deveria ser um objeto" );
        } else if ( empty( $body->text ) && empty( $request->file( 'photo' ) ) ) {
            throw new BadRequestException( "É obrigatório informar propriedade 'text' ou arquivo 'photo'" );
        }

        $post = new Post();
        $post->setText( isset( $body->text ) ? $body->text : null );
        $post->setAuthor( $request->user() );
        $post->setLocation( $this->parseLocation( $request ) );

        // Se uma foto foi enviada, vamos persisti-la também
        $photo_file = $request->file( 'photo' );
        if ( $photo_file != null ) {
            $photo = new PostPhoto();
            $photo->setType( $photo_file->getType() );
            $photo->setData( $photo_file->getContent() );
            $photo->setPost($post);

            $post->getPhotos()->add( $photo );
        }

        return $post;
    }

    /**
     * Cria um post e responde à requisição HTTP.
     *
     * @param   Post $post          O post para criar
     * @param   Response $response  A resposta HTTP para retornar o resultado da criação do post
     */
    protected final function createPost ( Post $post, Response $response ) {
        /** @var PostModel $post_model */
        $post_model = App::getInstance()->model( 'post' );

        $post_model->create( $post );
        $response->status( HttpStatus::HTTP_CREATED );
        $this->render( $post );
    }

    /**
     * Interpreta e retorna o objeto de localização correto
     *
     * @param   Request $request
     * @return  mixed
     */
    private function parseLocation ( Request $request ) {
        $location = @$request->body()->location;
        if ( !$location ) {
            return null;
        } else if ( is_string( $location ) ) {
            return json_decode( $location, true );
        } else if ( Utils::isAssociative( $location ) ) {
            return $location;
        } else if ( is_object( $location ) ) {
            return get_object_vars( $location );
        }

        return null;
    }
}