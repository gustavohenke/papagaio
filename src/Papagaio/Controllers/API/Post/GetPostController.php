<?php

namespace Papagaio\Controllers\API\Post;

use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Model\PostModel;

class GetPostController extends AbstractPostController implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app            A aplicação
     * @param   Request $request    A requisição atual
     * @param   Response $response  A resposta atual
     * @param   array $args         Argumentos recebidos na URL
     * @return  void
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        /** @var PostModel $post_model */
        $post_model = $app->model( 'post' );

        $id = $args[ 0 ];
        $post = $post_model->find( $id );

        $this->render( $post );
    }
}