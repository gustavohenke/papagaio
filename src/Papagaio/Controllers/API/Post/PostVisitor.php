<?php

namespace Papagaio\Controllers\API\Post;

use Papagaio\Entity\Like;
use Papagaio\Entity\Post;
use Papagaio\Entity\User;
use Papagaio\Entity\Visitor\DefaultVisitor;

class PostVisitor extends DefaultVisitor {

    /**
     * @var User
     */
    private $user;

    public function __construct ( User $user ) {
        $this->user = $user;
    }

    public function visitPost ( Post $post ) {
        $user = $this->user;

        $data = parent::visitPost( $post );
        $data[ 'liked' ] = $post->getLikes()->exists(function ( $index, Like $like ) use ( $user ) {
            return $like->getUser() === $user;
        });

        return $data;
    }

}