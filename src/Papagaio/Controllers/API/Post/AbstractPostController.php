<?php

namespace Papagaio\Controllers\API\Post;

use Papagaio\Controllers\Controller;
use Papagaio\Core\App;

abstract class AbstractPostController implements Controller {

    protected function render ( $entity ) {
        $app = App::getInstance();
        $user = $app->request()->user();
        $visitor = new PostVisitor( $user );

        $app->response()->render( $entity, [], $visitor );
    }

}