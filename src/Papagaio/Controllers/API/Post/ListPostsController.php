<?php

namespace Papagaio\Controllers\API\Post;

use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Model\PostModel;

class ListPostsController extends AbstractPostController implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app A aplicação
     * @param   Request $request A requisição atual
     * @param   Response $response A resposta atual
     * @param   array $args Argumentos recebidos na URL
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        /** @var PostModel $post_model */
        $post_model = $app->model( 'post' );

        // Se a query string 'all' estiver presente, nós devemos ignorar os usuários que o usuário
        // logado segue.
        $user = $request->query( 'all' ) ? null : $request->user();
        $posts = $post_model->findAll( $user );

        $response->count( $posts->count() );
        $this->render( $posts );
    }
}