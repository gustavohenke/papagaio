<?php

namespace Papagaio\Controllers\API\Post\Reply;

use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Model\PostModel;

class ListRepliesController implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app A aplicação
     * @param   Request $request A requisição atual
     * @param   Response $response A resposta atual
     * @param   array $args Argumentos recebidos na URL
     * @return  void
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        /** @var PostModel $post_model */
        $post_model = $app->model( 'post' );

        $post = $post_model->find( $args[ 0 ] );
        $replies = $post->getReplies();

        $response->count( $replies->count() );
        $response->render( $replies );
    }
}