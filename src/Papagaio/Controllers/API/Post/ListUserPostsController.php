<?php

namespace Papagaio\Controllers\API\Post;

use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Model\PostModel;
use Papagaio\Model\UserModel;

class ListUserPostsController extends AbstractPostController implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app A aplicação
     * @param   Request $request A requisição atual
     * @param   Response $response A resposta atual
     * @param   array $args Argumentos recebidos na URL
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        /** @var PostModel $post_model */
        $post_model = $app->model( 'post' );

        /** @var UserModel $user_model */
        $user_model = $app->model( 'user' );

        $query = $args[ 0 ];
        $user = $user_model->find( $query );
        $posts = $post_model->findAllByUser( $user );

        $response->count( $posts->count() );
        $this->render( $posts );
    }
}