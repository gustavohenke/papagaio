<?php

namespace Papagaio\Controllers\Report;

use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Model\Report\UserReportModel;

class SuggestedUsersReportController implements Controller {

    /**
     * Executa um request.
     *
     * @param   App $app            A aplicação
     * @param   Request $request    A requisição atual
     * @param   Response $response  A resposta atual
     * @param   array $args         Argumentos recebidos na URL
     * @return  void
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        /** @var UserReportModel $userReport */
        $userReport = $app->model( 'Report\UserReport' );
        $suggested = $userReport->execute( $request->user() );

        $response->count( $suggested->count() );
        $response->render( $suggested );
    }

}