<?php

namespace Papagaio\Controllers\Feed;

use Papagaio\Controllers\Controller;
use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Entity\Entity;
use Papagaio\Entity\Visitor\EntityVisitor;
use Papagaio\Entity\Visitor\FeedVisitor;
use Suin\RSSWriter\Channel;
use Suin\RSSWriter\Feed;
use Suin\RSSWriter\Item;

abstract class AbstractFeedController implements Controller {

    /**
     * O app.
     *
     * @var App
     */
    protected $app;

    /**
     * Lista de argumentos vindos da URL.
     *
     * @var array
     */
    protected $args;

    /**
     * @var EntityVisitor
     */
    private $visitor;

    /**
     * Executa um request.
     *
     * @param   App $app A aplicação
     * @param   Request $request A requisição atual
     * @param   Response $response A resposta atual
     * @param   array $args Argumentos recebidos na URL
     * @return  void
     */
    public function execute ( App $app, Request $request, Response $response, array $args ) {
        $this->visitor = new FeedVisitor();
        $this->app = $app;
        $this->args = $args;

        $feed = new Feed();
        $channel = $this->getChannel()->appendTo( $feed );

        $items = $this->getItems();
        $items = array_map( [ $this, 'mapEntity' ], $items );
        array_map( [ $channel, 'addItem' ], $items );

        $response->header( 'Content-Type', 'text/xml' );
        $response->body( $feed->render() );
    }

    /**
     * Mapeia uma entidade para um Item de feed
     *
     * @param   $item
     * @return  mixed
     */
    private function mapEntity ( $item ) {
        if ( $item instanceof Entity ) {
            return $item->accept( $this->visitor );
        }

        return null;
    }

    /**
     * Cria um canal RSS
     *
     * @return  Channel
     */
    protected abstract function getChannel();

    /**
     * Lista todos os itens do feed atual
     *
     * @return  array
     */
    protected abstract function getItems();

}