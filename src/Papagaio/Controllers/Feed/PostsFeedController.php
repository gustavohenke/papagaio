<?php

namespace Papagaio\Controllers\Feed;

use Papagaio\Entity\Post;
use Papagaio\Model\PostModel;
use Papagaio\Model\UserModel;
use Suin\RSSWriter\Channel;
use Suin\RSSWriter\Item;

class PostsFeedController extends AbstractFeedController {

    /**
     * Cria um canal RSS
     *
     * @return  Channel
     */
    protected function getChannel () {
        $user = $this->getUser();

        $channel = new Channel();
        $channel->title( 'Posts de @' . $user->getUsername() );
        $channel->description( 'Posts de @' . $user->getUsername() . ' no Papagaio' );
        $channel->url( $this->app->base() . $this->app->buildUrl( 'userPostsHTML', [
            'user' => $user->getUsername()
        ]));

        return $channel;
    }

    /**
     * Lista todos os itens do feed atual
     *
     * @return  array
     */
    protected function getItems () {
        /** @var PostModel $postModel */
        $postModel = $this->app->model( 'post' );
        return $postModel->findAllByUser( $this->getUser() )->toArray();
    }

    /**
     * Retorna o usuário da requisição
     *
     * @return  \Papagaio\Entity\User
     * @throws  \Papagaio\Exception\NotFoundException
     */
    private function getUser () {
        /** @var UserModel $userModel */
        $userModel = $this->app->model( 'user' );
        return $userModel->find( $this->args[ 0 ] );
    }

}