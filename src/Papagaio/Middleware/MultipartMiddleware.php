<?php

namespace Papagaio\Middleware;

use h4cc\Multipart\ParserSelector;
use Papagaio\Core\App;
use Papagaio\Core\File;
use Papagaio\Core\Request;
use Papagaio\Core\Response;

class MultipartMiddleware extends Middleware {

    /**
     * Interpreta corpo multipart/form-data em requests que não são POST.
     *
     * Middleware utilizado por causa do seguinte bug: https://bugs.php.net/bug.php?id=55815
     *
     * @param   App $app
     * @param   Request $request
     * @param   Response $response
     * @return  void
     */
    public function invoke ( App $app, Request $request, Response $response ) {
        // No PHP, apenas o POST é interpretado corretamente como multipart.
        if ( $request->isSafe() || $request->method() == 'POST' || $request->mediaType() !== 'multipart/form-data' ) {
            $this->next->call();
            return;
        }

        $parser_selector = new ParserSelector();
        $parser = $parser_selector->getParserForContentType( $request->header( 'Content-Type' ) );
        $multipart = $parser->parse( $request->originalBody() );
        $body = [];

        foreach ( $multipart as $block ) {
            $params = $this->findParams( $block );

            // Se nós temos uma propriedade 'filename', significa que este bloco é um arquivo
            // Em caso contrário, vamos jogar este valor no array que será usado como novo corpo
            // da requisição
            if ( isset( $params[ 'filename' ] ) ) {
                $file = new File();
                $file->setName( $params[ 'filename' ] );
                $file->setType( $block[ 'headers' ][ 'content-type' ][ 0 ] );
                $file->setSize( strlen( $block[ 'body' ] ) );
                $file->setContent( $block[ 'body' ] );

                $request->file( $params[ 'name' ], $file );
            } else {
                $body[ $params[ 'name' ] ] = $block[ 'body' ];
            }
        }

        // Finalmente, atualiza o corpo da requisição e chama o próximo middleware.
        $request->setBody( $body );
        $this->next->call();
    }

    /**
     * Encontra os parâmetros do bloco multipart
     *
     * @param   array $block
     * @return  array
     */
    private function findParams ( $block ) {
        $params = [];
        $disposition = $block[ 'headers' ][ 'content-disposition' ][ 0 ];
        $parts = preg_split( '/\s*[;,]\s*/', $disposition );

        foreach ( $parts as $part ) {
            $param_parts = explode( '=', $part );
            if ( count( $param_parts ) < 2 ) {
                continue;
            }

            $params[ $param_parts[ 0 ] ] = trim( $param_parts[ 1 ], '"' );
        }

        return $params;
    }
}