<?php

namespace Papagaio\Middleware;


use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;

abstract class Middleware extends \Slim\Middleware {

    /**
     * Invoca o middleware
     *
     * @param App $app
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public abstract function invoke ( App $app, Request $request, Response $response );

    public final function call () {
        $app = App::getInstance();
        $this->invoke( $app, $app->request(), $app->response() );
    }

}