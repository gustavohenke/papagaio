<?php

namespace Papagaio\Middleware;

use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;

class SecurityHeadersMiddleware extends Middleware {

    /**
     * Define headers de segurança, segundo recomendações da OWASP.
     *
     * @see     https://www.owasp.org/index.php/List_of_useful_HTTP_headers
     * @param   App $app
     * @param   Request $request
     * @param   Response $response
     * @return  void
     */
    public function invoke ( App $app, Request $request, Response $response ) {
        $response->header( 'Content-Security-Policy', "defaul-src 'self'" );
        $response->header( 'X-Content-Type-Options', 'nosniff' );
        $response->header( 'X-Frame-Options', 'deny' );
        $response->header( 'X-XSS-Protection', '1; mode=block' );

        $this->next->call();
    }
}