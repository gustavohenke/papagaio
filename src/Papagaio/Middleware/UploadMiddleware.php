<?php

namespace Papagaio\Middleware;

use Papagaio\Core\App;
use Papagaio\Core\File;
use Papagaio\Core\Request;
use Papagaio\Core\Response;

class UploadMiddleware extends Middleware {

    /**
     * Invoca o middleware
     *
     * @param App $app
     * @param Request $request
     * @param Response $response
     * @return mixed|void
     */
    public function invoke ( App $app, Request $request, Response $response ) {
        foreach ( $_FILES as $name => $file_data ) {
            $file = new File();
            $file->setName( $file_data[ 'name' ] );
            $file->setSize( $file_data[ 'size' ] );
            $file->setContent( file_get_contents( $file_data[ 'tmp_name' ] ) );
            $file->setType( $file_data[ 'type' ] );

            $request->file( $name, $file );
        }

        $this->next->call();
    }
}