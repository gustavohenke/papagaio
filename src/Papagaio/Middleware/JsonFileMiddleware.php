<?php

namespace Papagaio\Middleware;

use Papagaio\Core\App;
use Papagaio\Core\File;
use Papagaio\Core\Request;
use Papagaio\Core\Response;

class JsonFileMiddleware extends Middleware {

    /**
     * Invoca o middleware
     *
     * @param App $app
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public function invoke ( App $app, Request $request, Response $response ) {
        $body = $request->body();
        if ( !$body || is_array( $body ) ) {
            return $this->next->call();
        }

        foreach ( $body as $key => $value ) {
            $value = $this->toFile( $key, $value );
            if ( $value ) {
                $request->file( $key, $value );
            }
        }

        return $this->next->call();
    }

    /**
     * @param   string $name
     * @param   mixed $value
     * @return  File
     */
    private function toFile ( $name, $value ) {
        if ( !isset( $value->type ) && !isset( $value->content ) ) {
            return null;
        }

        // Usando a opção strict = true, podemos, com sorte, tornar a sring do conteúdo um pouco
        // mais válida.
        $content = base64_decode( $value->content, true );
        if ( !$content ) {
            return null;
        }

        $file = new File();
        $file->setName( $name );
        $file->setType( $value->type );
        $file->setContent( $content );
        $file->setSize( strlen( $content ) );

        return $file;
    }
}