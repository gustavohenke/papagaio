<?php

namespace Papagaio\Middleware;

use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Entity\User;
use Papagaio\Model\UserModel;

class SessionMiddleware extends Middleware {

    /**
     * Invoca o middleware
     *
     * @param   App $app
     * @param   Request $request
     * @param   Response $response
     * @return  void
     */
    public function invoke ( App $app, Request $request, Response $response ) {
        /** @var UserModel $user_model */
        $user_model = $app->model( 'user' );

        $authorization = $request->header( 'Authorization' );
        $user = false;

        // Realiza a autenticação. Opções:
        // - Bearer (token)
        // - Basic (usuário:senha)
        // Se algum destes métodos falhar, uma exception _pode_ ser gerada. Por isto o try/catch.
        try {
            if ( strpos( $authorization, 'Bearer ' ) === 0 ) {
                $token = str_replace( 'Bearer ', '', $authorization );
                $user = $user_model->findBySession( $token );
            } else if ( strpos( $authorization, 'Basic ' ) === 0 ) {
                $token = str_replace( 'Basic ', '', $authorization );
                list( $username, $password ) = explode( ':', base64_decode( $token ) );

                $user_login = new User();
                $user_login->setUsername( $username );
                $user_login->setPassword( $password );
                $user = $user_model->tempLogin( $user_login );
            }
        } catch ( \Exception $e ) {}

        // Define o usuário do request, se ele foi autenticado
        if ( $user ) {
            $request->user( $user );
        }

        $this->next->call();
    }
}