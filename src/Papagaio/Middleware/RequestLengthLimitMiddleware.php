<?php

namespace Papagaio\Middleware;

use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Exception\RequestTooLargeException;

class RequestLengthLimitMiddleware extends Middleware {

    /**
     * Invoca o middleware
     *
     * @param   App $app
     * @param   Request $request
     * @param   Response $response
     * @return  void
     * @throws  RequestTooLargeException
     */
    public function invoke ( App $app, Request $request, Response $response ) {
        $max = intval( $app->config()->get( 'requestLength' ) );
        $max_mb = $max * 1024 * 1024;
        $length = strlen( $request->originalBody() );

        if ( $length > $max_mb ) {
            throw new RequestTooLargeException( 'O tamanho máximo da requisição é de ' . $max . ' MB' );
        }

        $this->next->call();
    }
}