<?php

namespace Papagaio\Middleware;

use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;
use Papagaio\Exception\UnauthorizedException;

class AuthMiddleware extends Middleware {

    /**
     * Invoca o middleware
     *
     * @param   App $app
     * @param   Request $request
     * @param   Response $response
     * @return  void
     * @throws  UnauthorizedException
     */
    public function invoke ( App $app, Request $request, Response $response ) {
        if ( $request->user() == null ) {
            throw new UnauthorizedException();
        }

        $this->next->call();
    }
}