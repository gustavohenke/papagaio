<?php

namespace Papagaio\Middleware;

use Papagaio\Core\App;
use Papagaio\Core\Request;
use Papagaio\Core\Response;

class CorsMiddleware extends Middleware {

    /**
     * Invoca o middleware
     *
     * @param App $app
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public function invoke ( App $app, Request $request, Response $response ) {
        if ( strpos( $request->path(), '/api' ) === -1 ) {
            return $this->next->call();
        }

        $response->header( 'Access-Control-Allow-Credentials', 'true' );
        $response->header( 'Access-Control-Allow-Origin', '*' );

        if ( $request->method() === 'OPTIONS' ) {
            $response->header( 'Access-Control-Allow-Methods', join( ', ', [
                'GET',
                'POST',
                'PUT',
                'PATCH',
                'DELETE'
            ]));

            // $response->header( 'Access-Control-Allow-Headers', );
        }

        $this->next->call();
    }
}