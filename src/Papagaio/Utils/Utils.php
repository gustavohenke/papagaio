<?php

namespace Papagaio\Utils;

use Doctrine\Common\Collections\Collection;
use Papagaio\Entity\Entity;
use Papagaio\Entity\Visitor\DefaultVisitor;
use Papagaio\Entity\Visitor\EntityVisitor;

class Utils {

    /**
     * Detecta o MIME-type de um arquivo
     *
     * @param   string $path
     * @return  string
     */
    public static function getMimeType ( $path ) {
        $info = new \finfo( FILEINFO_MIME );
        return $info->file( $path );
    }

    /**
     * Determina se um array é associativo ou não.
     *
     * @param   array $arr
     * @return  bool
     */
    public static function isAssociative ( array $arr ) {
        return is_array( $arr ) && array_keys( $arr ) !== range( 0, count( $arr ) - 1);
    }

    /**
     * Determina se a requisição está acontecendo mediante HTTPS
     *
     * @return bool
     */
    public static function isHTTPS () {
        $is = isset( $_SERVER[ 'HTTPS' ] ) && $_SERVER[ 'HTTPS' ];
        if ( $is ) {
            return true;
        } else if ( isset( $_SERVER[ 'HTTP_CF_VISITOR' ] ) ) {
            $visitor = json_decode( $_SERVER[ 'HTTP_CF_VISITOR' ] );
            return $visitor->scheme === 'https';
        } else if ( isset( $_SERVER[ 'HTTP_X_FORWARDED_PROTO' ] ) ) {
            return strtolower( $_SERVER[ 'HTTP_X_FORWARDED_PROTO' ] ) === 'https';
        }

        return false;
    }

    /**
     * Transforma qualquer valor em uma string JSON.
     *
     * @param   mixed $value
     * @param   EntityVisitor $visitor
     * @return  string
     */
    public static function toJSON ( $value, EntityVisitor $visitor = null ) {
        if ( $visitor == null ) {
            $visitor = new DefaultVisitor();
        }

        $json = self::internalToJSON( $value, $visitor );
        return json_encode( $json );
    }

    private static function internalToJSON ( $value, EntityVisitor $visitor ) {
        $is_arr = is_array( $value );
        $is_obj = is_object( $value );

        if ( $is_arr && !self::isAssociative( $value ) ) {
            $count = count( $value );
            $arr = [];

            for ( $i = 0; $i < $count; $i++ ) {
                array_push( $arr, self::internalToJSON( $value[ $i ], $visitor ) );
            }

            return $arr;
        } else if ( !$is_arr && !$is_obj ) {
            return $value;
        } else if ( $value instanceof \DateTime ) {
            return $value->format( 'c' );
        } else if ( $value instanceof Collection ) {
            return Utils::internalToJSON( $value->toArray(), $visitor );
        }

        // Se o objeto implementa Entity, então vamos visitá-lo.
        if ( is_object( $value ) && $value instanceof Entity ) {
            $value = $value->accept( $visitor );
        }

        foreach ( $value as $key => &$val ) {
            $val = self::internalToJSON( $val, $visitor );
        }

        return $value;
    }

}