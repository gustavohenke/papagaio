<?php

require_once 'vendor/autoload.php';

use Papagaio\Core\App;
use Papagaio\Utils\Utils;

// Exibe todos erros exceto os de depreciação
error_reporting( E_ERROR | E_WARNING | E_PARSE | E_NOTICE );

// Altera o processo para o diretório atual
chdir( __DIR__ );

// Workaround pra que Slim + Heroku + CloudFlare funcionem com HTTPS
if ( Utils::isHTTPS() ) {
    $_SERVER[ 'HTTPS' ] = 'on';
    $_SERVER[ 'SERVER_PORT' ] = 443;
}

// Obtem a aplicação e inicializa-a
$app = App::getInstance();
$app->init();