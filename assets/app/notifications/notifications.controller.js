angular.module( "app.notifications" ).controller(
    "NotificationsController",
    NotificationsController
);

function NotificationsController ( $scope, $system, $timeout, notifications ) {
    var ctrl = this;

    ctrl.init = function () {
        $system.title( "Notificações" );

        $scope.$watch( "$system.loadNotifications.$promise", ctrl.load );
        ctrl.load().then( _.partial( $timeout, ctrl.clear, 5000 ) );
    };

    /**
     * Carrega a lista de notificações do usuário atual.
     *
     * @returns {Promise}
     */
    ctrl.load = function () {
        return $system.loadNotifications.$promise.then(function ( result ) {
            ctrl.list = result;
        });
    };

    /**
     * Marca todas as notificações como lidas.
     * Se não houver nenhuma não lida, não faz nada.
     *
     * @returns {Promise}
     */
    ctrl.clear = function () {
        var unread = _.find( ctrl.list, {
            unread: true
        });

        // Se for o caso de não ter encontrado nenhuma notificação não lida, não enviamos a
        // requisição para limpar o status no servidor.
        if ( !unread ) {
            return;
        }

        return notifications.patch();
    };

    // ---------------------------------------------------------------------------------------------

    ctrl.init();
}