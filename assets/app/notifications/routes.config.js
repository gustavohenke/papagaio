angular.module( "app.notifications" ).config( routesConfig );

function routesConfig ( $stateProvider ) {
    $stateProvider.state( "notifications", {
        url: "/notifications",
        controller: "NotificationsController",
        controllerAs: "$notifications",
        templateUrl: "app/notifications/notifications.html"
    });
}