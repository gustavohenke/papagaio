angular.module( "app.home" ).controller( "HomeController", HomeController );

function HomeController ( $scope ) {
    var ctrl = this;

    ctrl.init = function () {
        $scope.$on( "login",        ctrl.broadcast );
        $scope.$on( "login.done",   ctrl.broadcast );
        $scope.$on( "signup",       ctrl.broadcast );
        $scope.$on( "signup.done",  ctrl.broadcast );
    };

    ctrl.broadcast = function ( evt ) {
        if ( evt.targetScope === $scope ) {
            return;
        }

        $scope.$broadcast( evt.name );
    };

    // ---------------------------------------------------------------------------------------------

    ctrl.init();
}