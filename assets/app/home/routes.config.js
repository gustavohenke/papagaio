angular.module( "app.home" ).config( routesConfig );

function routesConfig ( $stateProvider ) {
    $stateProvider.state( "home", {
        url: "/",
        templateUrl: "app/home/home.html",
        data: {
            auth: false
        },
        views: {
            "": {
                controller: "HomeController",
                templateUrl: "app/home/home.html"
            },
            "signup@home": {
                controller: "SignupController",
                controllerAs: "$signup",
                templateUrl: "app/signup/signup.html"
            },
            "login@home": {
                controller: "LoginController",
                controllerAs: "$login",
                templateUrl: "app/login/login.html"
            }
        }
    });
}