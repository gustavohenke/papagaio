angular.module( "app.core" ).config( html5ModeConfig );

function html5ModeConfig ( $locationProvider ) {
    $locationProvider.html5Mode( true );
}