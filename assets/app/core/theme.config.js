angular.module( "app.core" ).config( themeConfig );

function themeConfig ( $mdThemingProvider ) {
    // Hue: 148º
    /*$mdThemingProvider.definePalette( "papagaio", {
        50:  "#DCF9E9",
        100: "#C7EDD8",
        200: "#B8E5C8",
        300: "#A8DDBE",
        400: "#99D6B3",
        500: "#80C29F",
        600: "#72AF8C",
        700: "#68A284",
        800: "#618E71",
        900: "#406350",
        A100: "#8AFFC2",
        A200: "#17FF9B",
        A400: "#00FF79",
        A700: "#00D163",
        contrastDefaultColor: "light",
        contrastDarkColors: [ "50", "100", "200", "300", "A100", "A200" ],
        contrastStrongLightColors: [ "400", "500", "600", "700", "800", "900" ]
    });*/

    // Hue: 144º
    $mdThemingProvider.definePalette( "papagaio", {
        50:  "#D0F4DE",
        100: "#BAF7D2",
        200: "#9DF9C2",
        300: "#81EFAD",
        400: "#5EEA99",
        500: "#57D389",
        600: "#4DBC7A",
        700: "#44A56B",
        800: "#3A8E5C",
        900: "#28603E",
        A100: "#69FFA6",
        A200: "#5EEA99",
        A400: "#00FF67",
        A700: "#00CC52",
        contrastDefaultColor: "light",
        contrastDarkColors: [ "50", "100", "200", "300", "A100", "A200" ],
        contrastStrongLightColors: [ "400", "500", "600", "700", "800", "900" ]
    });

    $mdThemingProvider.definePalette( "search", {
        50:     "#ffffff",
        100:    "#ffffff",
        200:    "#ffffff",
        300:    "#ffffff",
        400:    "#ffffff",
        500:    "#ffffff",
        600:    "#ffffff",
        700:    "#ffffff",
        800:    "#ffffff",
        900:    "#ffffff",
        A100:   "#ffffff",
        A200:   "#ffffff",
        A400:   "#ffffff",
        A700:   "#ffffff",
        contrastDefaultColor: "dark"
    });

    $mdThemingProvider.theme( "default" ).primaryPalette( "papagaio", {
        default: "600"
    }).accentPalette( "amber" );

    $mdThemingProvider.theme( "search" ).primaryPalette( "search" );
}