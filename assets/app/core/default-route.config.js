angular.module( "app.core" ).config( defaultRouteConfig );

function defaultRouteConfig ( $urlRouterProvider ) {
    $urlRouterProvider.otherwise( "/" );
}