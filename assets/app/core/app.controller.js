angular.module( "app.core" ).controller( "AppController", AppController );

function AppController ( $rootScope, $state, $mdMedia, $system ) {
    $rootScope.$system = $system;
    $rootScope.$state = $state;
    $rootScope.$mdMedia = $mdMedia;
}