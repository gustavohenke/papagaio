angular.module( "app.core" ).service( "routesProtector", RoutesProtectorService );

function RoutesProtectorService ( $rootScope, $auth, $state ) {
    this.init = function ( loggedState, guestState ) {
        $rootScope.$on( "$stateChangeStart", function ( evt, to ) {
            var handled;
            var isLogged = $auth.isAuthenticated();
            var data = to.data || {};

            if ( isLogged && data.auth === false ) {
                handled = $state.go( loggedState );
            } else if ( !isLogged && data.auth === true ) {
                handled = $state.go( guestState );
            }

            handled && evt.preventDefault();
        });
    };
}