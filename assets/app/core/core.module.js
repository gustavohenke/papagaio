angular.module( "app.core", [
    // 3rd party
    "ngMaterial",
    "ngMessages",
    "restangular",
    "satellizer",
    "ui.router",
    "file-model",

    // Compartilhados
    "shared.auth",
    "shared.data",
    "shared.postForm",
    "shared.postsList",
    "shared.profile",
    "shared.system",
    "shared.toolbar",
    "shared.utils"
]).run( runBlock );

function runBlock ( routesProtector ) {
    routesProtector.init( "timeline", "home" );
}