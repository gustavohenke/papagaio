angular.module( "app.signup" ).controller( "SignupController", SignupController );

function SignupController ( $scope, $mdToast, $auth, $state, users ) {
    var ctrl = this;

    /**
     * Expressão regular pra validar nomes de usuário.
     * @type {RegExp}
     */
    ctrl.usernameRegex = /^[a-z0-9_.]+$/i;

    /**
     * Inicializa o controller
     */
    ctrl.init = function () {
        $scope.$on( "signup", function () {
            ctrl.loading = true;
        });

        $scope.$on( "signup.done", function () {
            ctrl.loading = false;
        });

        $scope.$on( "login", function () {
            ctrl.disabled = true;
        });

        $scope.$on( "login.done", function () {
            ctrl.disabled = false;
        });
    };

    /**
     * Cria um usuário.
     * Se o formulário estiver inválido, não faz nada.
     *
     * @event   signup
     * @event   signup.done
     * @return  {Promise}
     */
    ctrl.submit = function () {
        if ( ctrl.form.$invalid ) {
            return;
        }

        // Um toast já existe?
        if ( ctrl.toast ) {
            // Sim: então devemos fechar ele
            $mdToast.hide( ctrl.toast );
        }

        // Emite o evento signup para iniciar o processamento
        $scope.$emit( "signup" );
        return users.post( ctrl.user ).then(function () {
            return $auth.login( ctrl.user );
        }).then(function () {
            return $state.go( "timeline" );
        }).catch(function ( err ) {
            var toast = $mdToast.simple().content( err.data.message );
            ctrl.toast = $mdToast.show( toast ).then( function () {
                delete ctrl.toast;
            });

            // Emite o evento signup.done para concluir o carregamento
            $scope.$emit( "signup.done" );
        });
    };

    // ---------------------------------------------------------------------------------------------

    ctrl.init();
}