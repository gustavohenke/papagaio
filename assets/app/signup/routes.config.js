angular.module( "app.login" ).config( routesConfig );

function routesConfig ( $stateProvider ) {
    $stateProvider.state( "signup", {
        url: "/signup",
        data: {
            auth: false
        },
        views: {
            "": {
                templateUrl: "app/signup/signup-wrapper.html"
            },
            "form@signup": {
                templateUrl: "app/signup/signup.html",
                controller: "SignupController",
                controllerAs: "$signup"
            }
        }
    });
}