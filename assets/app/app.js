angular.module( "app", [
    "app.core",
    "app.config",
    "app.home",
    "app.login",
    "app.notifications",
    "app.post",
    "app.profile",
    "app.signup",
    "app.timeline"
]);