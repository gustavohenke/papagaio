angular.module( "app.timeline" ).controller( "UserSuggestionController", UserSuggestionController );

function UserSuggestionController ( userSuggestion ) {
    var ctrl = this;

    ctrl.init = function () {
        ctrl.load();
    };

    ctrl.load = function () {
        userSuggestion.getList().then(function ( suggestions ) {
            ctrl.suggestions = suggestions;
        }, function ( err ) {
            ctrl.error = err.data;
        });
    };

    // ---------------------------------------------------------------------------------------------

    this.init();
}