angular.module( "app.timeline" ).controller( "TimelineController", TimelineController );

function TimelineController ( $timeout, $filter, $stateParams, $system, posts, hashtagPosts ) {
    var ctrl = this;
    var hashtagFilter = $filter( "hashtag" );

    /**
     * Inicializa o controller.
     * Carrega os posts para a timeline (usuários seguidos ou hashtag)
     */
    ctrl.init = function () {
        ctrl.hashtag = $stateParams.hashtag;

        if ( ctrl.hashtag ) {
            $system.title( "HashTag " + hashtagFilter( ctrl.hashtag ) + " no Papagaio" );
            ctrl.loadHashTag( ctrl.hashtag );
        } else {
            ctrl.load();
        }
    };

    /**
     * Carrega posts da timeline do usuário logado
     */
    ctrl.load = function () {
        ctrl.loading = true;
        posts.getList().then(function ( posts ) {
            $timeout(function () {
                ctrl.posts = posts;
            });
        }, function ( err ) {
            ctrl.error = err;
        }).finally(function () {
            ctrl.loading = false;
        });
    };

    /**
     * Carrega posts de uma hashtag
     * @param   {String} tag
     */
    ctrl.loadHashTag = function ( tag ) {
        hashtagPosts( tag ).getList().then(function ( posts ) {
            ctrl.posts = posts;
        }, function ( err ) {
            ctrl.error = err;
        });
    };

    // ---------------------------------------------------------------------------------------------

    this.init();
}