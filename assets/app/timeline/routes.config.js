angular.module( "app.timeline" ).config( routesConfig );

function routesConfig ( $stateProvider ) {
    var views = function ( name ) {
        var obj = {
            "": {
                templateUrl: "app/timeline/timeline.html",
                controller: "TimelineController",
                controllerAs: "$timeline"
            }
        };
        obj[ "suggestions@" + name ] = {
            templateUrl: "app/timeline/user-suggestions.html",
            controller: "UserSuggestionController",
            controllerAs: "$suggestions"
        };

        return obj;
    };

    $stateProvider.state( "timeline", {
        url: "/",
        data: {
            auth: true
        },
        views: views( "timeline" )
    });

    $stateProvider.state( "hashtag", {
        url: "/hashtag/:hashtag",
        data: {
            auth: true
        },
        views: views( "hashtag" )
    });
}