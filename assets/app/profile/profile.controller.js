angular.module( "app.profile" ).controller( "ProfileController", ProfileController );

function ProfileController ( $stateParams, $system, users, userPosts ) {
    var User;
    var ctrl = this;

    /**
     * Inicializa o controller
     * Carrega perfil e posts do usuário.
     */
    ctrl.init = function () {
        User = users.one( $stateParams.user );

        ctrl.memberSinceFormat = "MMMM 'de' yyyy";

        ctrl.loadUser();
        ctrl.loadPosts();
    };

    /**
     * Carrega o usuário do perfil acessado
     */
    ctrl.loadUser = function () {
        var getUser = function () {
            return User.get();
        };

        return $system.$promise.then( getUser, getUser ).then(function ( user ) {
            // Se o usuário logado é o usuário do perfil que estamos acessando,
            // então não iremos detectar se está seguindo a si mesmo!
            ctrl.user = user;
        }, function ( err ) {
            ctrl.error = err.data;
            ctrl.error.user = {
                username: $stateParams.user
            };
        });
    };

    /**
     * Carrega os posts do perfil acessado
     */
    ctrl.loadPosts = function () {
        userPosts( $stateParams.user ).getList().then(function ( posts ) {
            ctrl.posts = posts;
        });
    };

    // ---------------------------------------------------------------------------------------------

    ctrl.init();
}