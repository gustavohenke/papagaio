angular.module( "app.profile" ).config( routesConfig );

function routesConfig ( $stateProvider ) {
    $stateProvider.state( "profile", {
        url: "/user/:user",
        controller: "ProfileController",
        controllerAs: "$profile",
        templateUrl: "app/profile/profile.html"
    });
}