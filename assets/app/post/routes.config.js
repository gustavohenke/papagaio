angular.module( "app.post" ).config( routesConfig );

function routesConfig ( $stateProvider ) {
    $stateProvider.state( "post", {
        url: "/post/:id",
        templateUrl: "app/post/post.html",
        controller: "PostController",
        controllerAs: "$post"
    });
}