angular.module( "app.post" ).controller( "PostController", PostController );

function PostController ( $stateParams, posts ) {
    var ctrl = this;

    /**
     * Inicializa o controller
     */
    ctrl.init = function () {
        ctrl.dateFormat = "dd 'de' MMM 'de' yyyy 'às' HH:mm";

        ctrl.load();
    };

    ctrl.load = function () {
        posts.one( $stateParams.id ).get().then(function ( post ) {
            ctrl.post = post;
        });
    };

    // ---------------------------------------------------------------------------------------------

    ctrl.init();
}