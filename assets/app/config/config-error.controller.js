angular.module( "app.config" ).controller( "ConfigErrorController", ConfigErrorController );

// Este controller não tem funcionalidade alguma, pois é utilizado para um toast de erro.
// E, nas configurações do toast a mensagem de erro já é passada através da propriedade 'locals',
// então não há porque implementar algo aqui.
function ConfigErrorController () {}