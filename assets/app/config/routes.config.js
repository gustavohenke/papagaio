angular.module( "app.config" ).config( routesConfig );

function routesConfig ( $stateProvider ) {
    $stateProvider.state( "config", {
        url: "/config",
        controller: "ConfigController",
        controllerAs: "$config",
        templateUrl: "app/config/config.html",
        data: {
            auth: true
        },
        resolve: {
            system: function ( $system ) {
                return $system.$promise;
            }
        }
    });
}