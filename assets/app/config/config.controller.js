angular.module( "app.config" ).controller( "ConfigController", ConfigController );

function ConfigController ( $mdToast, $system, users ) {
    var ctrl = this;

    /**
     * Inicializa o controller.
     */
    ctrl.init = function () {
        $system.title( "Configurações" );
        $system.isForm( true );
        $system.formAction( ctrl.submit );

        ctrl.data = {
            username: $system.user.username,
            email: $system.user.email
        };
    };

    /**
     * Envia os dados de configurações.
     *
     * @returns {Promise}
     */
    ctrl.submit = function () {
        var data;
        if ( ctrl.form.$invalid ) {
            return;
        }

        data = new FormData();
        _.forEach( ctrl.data, function ( value, key ) {
            data.append( key, value );
        });

        $system.loading( true );
        return users.withHttpConfig({
            transformRequest: angular.identity
        }).one( "me" ).customPUT( data, null, null, {
            "Content-Type": undefined
        }).then(function () {
            $system.back();
        }, function ( err ) {
            ctrl.form.submiting = false;
            $mdToast.show({
                templateUrl: "app/config/config-error.html",
                position: "top right",
                controller: "ConfigErrorController",
                controllerAs: "$error",
                bindToController: true,
                locals: {
                    message: err.data.message
                }
            });
        }).finally(function () {
            $system.loading( false );
        });
    };

    // ---------------------------------------------------------------------------------------------

    ctrl.init();
}