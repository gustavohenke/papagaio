angular.module( "app.login" ).config( routesConfig );

function routesConfig ( $stateProvider ) {
    $stateProvider.state( "login", {
        url: "/login",
        data: {
            auth: false
        },
        views: {
            "": {
                templateUrl: "app/login/login-wrapper.html"
            },
            "form@login": {
                templateUrl: "app/login/login.html",
                controller: "LoginController",
                controllerAs: "$login"
            }
        }
    });
}