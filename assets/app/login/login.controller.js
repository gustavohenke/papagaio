angular.module( "app.login" ).controller( "LoginController", LoginController );

function LoginController ( $scope, $mdToast, $auth, $state ) {
    var ctrl = this;

    /**
     * Inicializa o controller
     */
    ctrl.init = function () {
        $scope.$on( "signup", function () {
            ctrl.disabled = true;
        });

        $scope.$on( "signup.done", function () {
            ctrl.disabled = false;
        });

        $scope.$on( "login", function () {
            ctrl.loading = true;
        });

        $scope.$on( "login.done", function () {
            ctrl.loading = false;
        });
    };

    /**
     * Realiza o login
     * Se o formulário estiver inválido, não faz nada.
     *
     * @event       login
     * @event       login.done
     * @returns    {Promise}
     */
    ctrl.submit = function () {
        if ( ctrl.form.$invalid ) {
            return;
        }

        $scope.$emit( "login" );
        return $auth.login({
            username: ctrl.username,
            password: ctrl.password
        }).then(function () {
            $state.go( "timeline" );
        }, function ( err ) {
            var toast = $mdToast.simple().content( err.data.message );
            $mdToast.show( toast );

            $scope.$emit( "login.done" );
        });
    };

    // ---------------------------------------------------------------------------------------------

    ctrl.init();
}