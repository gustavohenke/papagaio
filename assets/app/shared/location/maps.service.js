angular.module( "shared.location" ).service( "maps", mapsService );

function mapsService ( $q, $window, $document ) {
    var maps, load;
    var svc = this;

    /**
     * Determina se a API Geolocation está habilitada
     *
     * @returns {boolean}
     */
    svc.enabled = function () {
        return "geolocation" in navigator;
    };

    /**
     * Retorna a posição atual (latitude/longitude) + nome do local
     *
     * @returns {Promise}
     */
    svc.getPosition = function () {
        var load;
        var deferred = $q.defer();

        // Quick fail: sem API Geolocation
        if ( !svc.enabled() ) {
            return deferred.reject( new Error( "API Geolocation desativada." ) );
        }

        // Obtemos a API do Maps enquanto consultamos ao browser a localização atual.
        // Caso demore pro usuário responder, a API pode ir carregando neste meio tempo.
        load = svc.load();
        navigator.geolocation.getCurrentPosition(function ( position ) {
            deferred.resolve( position.coords );
        }, function () {
            deferred.reject( new Error( "Erro ao obter posição" ) );
        });

        return deferred.promise.then(function ( coords ) {
            return svc.getAddress( coords );
        });
    };

    /**
     * Faz o geocoding para a localização informada
     *
     * @param   {Object} coords
     * @returns {Promise}
     */
    svc.getAddress = function ( coords ) {
        return svc.load().then(function () {
            var deferred = $q.defer();
            var geocoder = new maps.Geocoder();

            geocoder.geocode({
                location: {
                    lat: coords.latitude,
                    lng: coords.longitude
                }
            }, function ( results, status ) {
                var addr;
                if ( status !== maps.GeocoderStatus.OK ) {
                    return deferred.reject(
                        new Error( "Não foi possível encontrar a posição atual." )
                    );
                }

                // Usamos um resultado de localidade (aka cidade), pois traz o nível de
                // informação que precisamos (sem ruas/estradas, mas também não só estado/país).
                addr = _.find( results, {
                    types: [ "locality" ]
                });

                // Vamos pra área administrativa de nível 2 (cidade pro BR)
                addr = addr || _.find( results, {
                    types: [ "administrative_area_level_2" ]
                });

                deferred.resolve({
                    latitude:       coords.latitude,
                    longitude:      coords.longitude,
                    description:    addr.formatted_address
                });
            });

            return deferred.promise;
        });
    };

    /**
     * Carrega a API do Google Maps.
     *
     * @returns {Promise}
     */
    svc.load = function () {
        var script;
        var deferred = $q.defer();

        // Retornamos a promise já existente se tivermos ela
        if ( load ) {
            return load;
        }

        // Já temos a API do Maps disponível?
        if ( !$window.google || !$window.google.maps ) {
            script = $document[ 0 ].createElement( "script" );

            $window.mapsCb = function () {
                delete $window.mapsCb;
                maps = $window.google.maps;

                deferred.resolve();
            };

            script.src = "https://maps.googleapis.com/maps/api/js?v=3.exp" +
                         "&sensor=false" +
                         "&callback=mapsCb";
            $document.find( "head" ).append( script );
        }

        return load = deferred.promise;
    };
}