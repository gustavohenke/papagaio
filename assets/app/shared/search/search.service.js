angular.module( "shared.search" ).service( "search", searchService );

function searchService ( $q, $filter, users, hashtags ) {
    var hashtagFilter = $filter( "hashtag" );
    var usernameFilter = $filter( "username" );

    /**
     * Pesquisa usuários
     * @param   {String} term
     * @returns {Promise}
     */
    this.users = function ( term ) {
        return users.getList({
            search: term
        }).then(function ( users ) {
            return users.map(function ( user ) {
                return {
                    display: usernameFilter( user ),
                    state: {
                        name: "profile",
                        params: {
                            user: user.username
                        }
                    }
                };
            });
        });
    };

    /**
     * Pesquisa hashtags
     * @param   {String} term
     * @returns {Promise}
     */
    this.hashtags = function ( term ) {
        return hashtags.getList({
            search: term
        }).then(function ( tags ) {
            return tags.map(function ( tag ) {
                return {
                    display: hashtagFilter( tag.id ),
                    state: {
                        name: "hashtag",
                        params: {
                            hashtag: tag.id
                        }
                    }
                };
            });
        });
    };

    /**
     * Pesquisa todas as coisas
     * @param   {String} term
     * @returns {Promise}
     */
    this.all = function ( term ) {
        return $q.all([
            this.users( term ),
            this.hashtags( term )
        ]).then(function ( results ) {
            return results.reduce(function ( prev, next ) {
                return prev.concat( next );
            }, [] );
        });
    };
}