angular.module( "shared.system" ).service( "$system", $systemService );

function $systemService ( $rootScope, $q, $window, $interval, $auth, session, notifications ) {
    var svc = this;
    var deferred = $q.defer();

    svc.$promise = deferred.promise;

    $rootScope.$watch(function () {
        return $auth.getToken();
    }, function ( token ) {
        if ( !token ) {
            return;
        }

        session.one( token ).get().then(function ( session ) {
            deferred.resolve();
            svc.session = session;
            svc.user = session.user;
        }, function () {
            deferred.reject();
        });
    });

    // Limpa os valores do service ao iniciar a troca de estado
    $rootScope.$on( "$stateChangeStart", function () {
        svc.title( null );
        svc.isForm( false );
        svc.formAction( null );
    });

    /**
     * Inicializa o serviço system
     */
    svc.init = function () {
        // Carrega as notificações agora, e agenda a cada 10s para repetir a requisição
        svc.loadNotifications();
        $interval( svc.loadNotifications, 10000 );
    };

    /**
     * Carrega a lista de notificações do usuário
     * @returns {Promise}
     */
    svc.loadNotifications = function () {
        var promise;

        if ( !$auth.isAuthenticated() ) {
            return;
        }

        promise = svc.loadNotifications.$promise = notifications.getList();
        return promise.then(function ( response ) {
            svc.unreadNotifications = _.where( response, {
                unread: true
            }).length;
            return svc.notifications = response;
        });
    };

    /**
     * Volta para a página anterior.
     */
    svc.back = function () {
        $window.history.back();
    };

    /**
     * Retorna a URL base do sistema.
     *
     * @returns {String}
     */
    svc.baseUrl = function () {
        return document.querySelector( "base" ).getAttribute( "href" ).replace( /\/$/, "" );
    };

    /**
     * Obtem ou define o status de carregamento da página.
     *
     * @param   {Boolean} [value]
     * @returns {Boolean|$system}
     */
    svc.loading = function ( value ) {
        if ( value === undefined ) {
            return svc.$loading;
        }

        svc.$loading = !!value;
        return svc;
    };

    /**
     * Obtem ou define o título da página.
     *
     * Se um valor tiver sido passado, este método irá alterar o título da página.
     * Do contrário, irá retornar o título atual.
     *
     * @param   {String} [value]
     * @returns {String|$system}
     */
    svc.title = function ( value ) {
        if ( value === undefined ) {
            return svc.$title;
        }

        svc.$title = value;
        return svc;
    };

    /**
     * Obtem ou define o status de formulário da página atual.
     *
     * @param   {Boolean} [value]
     * @returns {Boolean|$system}
     */
    svc.isForm = function ( value ) {
        if ( value === undefined ) {
            return svc.$isForm;
        }

        svc.$isForm = !!value;
        return svc;
    };

    /**
     * Obtem ou define a ação executada pelo formulário da página atual.
     *
     * @param   {Function} [action]
     * @returns {Function|$system}
     */
    svc.formAction = function ( action ) {
        if ( action === undefined ) {
            return svc.$formAction;
        }

        svc.$formAction = action;
        return svc;
    };

    // ---------------------------------------------------------------------------------------------

    svc.init();
}