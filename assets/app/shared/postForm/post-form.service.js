angular.module( "shared.postForm" ).service( "postForm", postFormService );

function postFormService ( $mdDialog ) {
    var svc = this;

    /**
     * Exibe o formulário de post
     *
     * @param   {Object} event  O evento que deu origem à abertura do formulário
     */
    svc.show = function ( event ) {
        return svc.dialog = $mdDialog.show({
            targetEvent: event,
            controller: "PostFormController",
            controllerAs: "$post",
            templateUrl: "app/shared/postForm/post-form.html",
            clickOutsideToClose: true
        });
    };

    /**
     * Oculta o formulário de post, resolve a promise
     */
    svc.hide = function () {
        $mdDialog.hide();
    };

    /**
     * Fecha o formulário de post, rejeita a promise
     */
    svc.close = function () {
        $mdDialog.cancel();
    };
}