angular.module( "shared.postForm" ).directive( "locationButton", locationButtonDirective );

function locationButtonDirective ( maps ) {
    var dfn = {};

    dfn.restrict = "E";
    dfn.templateUrl = "app/shared/postForm/location-button.html";
    dfn.require = [ "ngModel", "locationButton" ];
    dfn.controller = "LocationButtonController";
    dfn.controllerAs = "$loc";

    dfn.link = function ( scope, element, attrs, ctrl ) {
        var ngModel = ctrl[ 0 ];
        var $loc = ctrl[ 1 ];
        $loc.ngModel = ngModel;

        // Não faremos nada com o elemento se a API geolocation não estiver disponível
        if ( !maps.enabled() ) {
            element.remove();
            return scope.$destroy();
        }
    };

    return dfn;
}