angular.module( "shared.postForm" ).controller(
    "LocationButtonController",
    LocationButtonController
);

function LocationButtonController ( maps ) {
    var ctrl = this;

    ctrl.find = function () {
        ctrl.loading = true;
        return maps.getPosition().then(function ( pos ) {
            ctrl.pos = pos;
            ctrl.ngModel.$setViewValue( pos );
        }, function () {
            ctrl.error = true;
        }).finally(function () {
            ctrl.loading = false;
        });
    };
}