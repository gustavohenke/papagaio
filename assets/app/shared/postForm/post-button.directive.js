angular.module( "shared.postForm" ).directive( "postButton", postButtonDirective );

function postButtonDirective () {
    var dfn = {};

    dfn.templateUrl = "app/shared/postForm/post-button.html";
    dfn.controller = "PostButtonController";
    dfn.controllerAs = "$postButton";

    dfn.link = function ( scope, element, attrs, $postButton ) {
        $postButton.style = element.hasClass( "post-button-fab" ) ? "fab" : "default";
    };

    return dfn;
}