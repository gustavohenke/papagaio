angular.module( "shared.postForm" ).controller( "PostFormController", PostFormController );

function PostFormController ( $state, posts, postForm ) {
    var ctrl = this;

    ctrl.init = function () {
        ctrl.hasGeolocation = "geolocation" in navigator;
    };

    /**
     * Fecha o formulário (cancela criação de post)
     */
    ctrl.close = function () {
        postForm.close();
    };

    /**
     * Verifica se a foto é válida
     *
     * @returns {boolean}
     */
    ctrl.photoIsValid = function () {
        var imageType = /^image\//;

        return ctrl.post.photo && imageType.test( ctrl.post.photo.type );
    };

    /**
     * Verifica se o post é válido
     * @returns {boolean}
     */
    ctrl.isValid = function () {
        if ( !ctrl.post ) {
            return false;
        }

        return ctrl.post.text || ctrl.photoIsValid();
    };

    /**
     * Cria o post, e após fecha o formulário.
     *
     * @returns {Promise}
     */
    ctrl.submit = function () {
        var data;

        if ( !ctrl.isValid() ) {
            return;
        }

        data = new FormData();
        _.forEach( ctrl.post, function ( value, name ) {
            var obj = _.isPlainObject( value ) || _.isArray( value );
            data.append( name, obj ? JSON.stringify( value ) : value );
        });

        ctrl.form.submiting = true;
        return posts.withHttpConfig({
            transformRequest: angular.identity
        }).post( data, null, {
            "Content-Type": undefined
        }).then( function () {
            postForm.hide();
            return $state.reload();
        }, function () {
            ctrl.form.submiting = false;
        });
    };

    // ---------------------------------------------------------------------------------------------

    ctrl.init();
}