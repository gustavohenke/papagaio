angular.module( "shared.utils" ).filter( "username", usernameFilter );

function usernameFilter () {
    return function ( input ) {
        return input && input.username && "@" + input.username;
    };
}