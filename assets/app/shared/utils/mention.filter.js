angular.module( "shared.utils" ).filter( "mention", mentionFilter );

function mentionFilter ( $sce, $state ) {
    return function ( text, post ) {
        text = text.toString();
        text = ( text || "" ).replace( /(@[\w\d]+)/gi, function ( user ) {
            var url, mentionIndex;

            // Se temos o objeto auxiliar do post, então vamos procurar nele a menção para o
            // respectivo usuário. Não achando nada, simplesmente não colocaremos o link no texto
            // do post.
            if ( post ) {
                mentionIndex = _.findIndex( post.mentions, {
                    username: user.substr( 1 )
                });

                if ( !~mentionIndex ) {
                    return user;
                }
            }

            url = $state.href( "profile", {
                user: user.substr( 1 )
            });

            return "<a href='" + url + "'>" + user + "</a>";
        });

        return $sce.trustAsHtml( text );
    };
}