angular.module( "shared.utils" ).filter( "hashtag", hashtagFilter );

function hashtagFilter ( $sce, $state ) {
    return function ( text, link ) {
        if ( !link ) {
            return "#" + text;
        }

        text = ( text || "" ).replace( /(&?#[\w\d]+;?)/gi, function ( tag ) {
            var url;
            var prefix = tag[ 0 ] === "&" ? "&" : "";
            var suffix = tag.substr( -1 ) === ";" ? ";" : "";

            // Se tem um & e um ;, então isso é uma entidade HTML.
            // Devemos deixar este cara intacto.
            if ( prefix && suffix ) {
                return tag;
            }

            url = $state.href( "hashtag", {
                hashtag: tag.substr( 1 ).toLowerCase()
            });

            return prefix + "<a href='" + url + "'>" + tag + "</a>" + suffix;
        });

        return $sce.trustAsHtml( text );
    };
}