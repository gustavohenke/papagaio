angular.module( "shared.utils" ).filter( "timeAgo", timeAgoFilter );

function timeAgoFilter () {
    return function ( input ) {
        var today = moment();
        var date = moment( input );

        if ( today.isSame( date, 'day' ) ) {
            return date.format( 'HH:mm' );
        } else if ( today.isSame( date, 'year' ) ) {
            return date.format( 'DD/MM' );
        } else {
            return date.format( 'DD/MM/YYYY' );
        }
    };
}