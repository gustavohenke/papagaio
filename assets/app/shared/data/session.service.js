angular.module( "shared.data" ).factory( "session", sessionService );

function sessionService ( Restangular ) {
    return Restangular.service( "session" );
}