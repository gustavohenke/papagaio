angular.module( "shared.data" ).factory( "posts", postsService );

function postsService ( Restangular ) {
    return Restangular.service( "post" );
}