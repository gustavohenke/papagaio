angular.module( "shared.data" ).config( restConfig );

function restConfig ( RestangularProvider ) {
    RestangularProvider.setBaseUrl( "/api" );
}