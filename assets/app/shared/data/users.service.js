angular.module( "shared.data" ).factory( "users", usersService );

function usersService ( Restangular ) {
    return Restangular.service( "user" );
}