angular.module( "shared.data" ).factory( "userSuggestion", userSuggestionService );

function userSuggestionService ( Restangular ) {
    return Restangular.service( "suggested-user", Restangular.all( "report" ) );
}