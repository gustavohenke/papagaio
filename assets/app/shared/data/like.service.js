angular.module( "shared.data" ).factory( "likes", likesService );

function likesService ( Restangular ) {
    return function ( post ) {
        return Restangular.one( "post", post ).one( "like" );
    };
}