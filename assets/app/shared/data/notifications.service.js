angular.module( "shared.data" ).factory( "notifications", notificationsService );

function notificationsService ( Restangular ) {
    return Restangular.service( "notification" );
}