angular.module( "shared.data" ).factory( "hashtagPosts", hashtagPostsService );

function hashtagPostsService ( Restangular ) {
    return function ( tag ) {
        return Restangular.service( "post", Restangular.one( "hashtag", tag ) );
    };
}