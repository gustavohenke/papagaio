angular.module( "shared.data" ).factory( "following", followingService );

function followingService ( Restangular ) {
    return Restangular.service( "following", Restangular.one( "user", "me" ) );
}