angular.module( "shared.data" ).factory( "userPosts", userPostsService );

function userPostsService ( Restangular, users ) {
    return function ( user ) {
        return Restangular.service( "post", users.one( user ) );
    };
}