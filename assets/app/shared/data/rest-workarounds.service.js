angular.module( "shared.data" )
       .factory( "restWorkarounds", restWorkaroundsService )
       .run( runWorkarounds );

function runWorkarounds ( restWorkarounds ) {
    restWorkarounds.init();
}

function restWorkaroundsService ( Restangular ) {
    function init () {
        Restangular.service = _.wrap( Restangular.service, function ( service, route, parent ) {
            var collection = Restangular.all( route );
            var svc = service( route, parent );
            svc.withHttpConfig = collection.withHttpConfig;
            svc.patch = collection.patch;

            return svc;
        });
    }

    return {
        init: init
    };
}