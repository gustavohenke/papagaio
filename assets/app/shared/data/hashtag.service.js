angular.module( "shared.data" ).factory( "hashtags", hashtagsService );

function hashtagsService ( Restangular ) {
    return Restangular.service( "hashtag" );
}