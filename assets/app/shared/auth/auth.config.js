angular.module( "shared.auth" ).config( authConfig );

function authConfig ( $authProvider ) {
    $authProvider.tokenName = "id";
    $authProvider.loginUrl = "/api/session";
}