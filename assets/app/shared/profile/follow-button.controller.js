angular.module( "shared.profile" ).controller( "FollowButtonController", FollowButtonController );

function FollowButtonController ( $scope, $system, following ) {
    var ctrl = this;

    ctrl.init = function () {
        var unregister = $scope.$watch(function () {
            return ctrl.user;
        }, function ( user ) {
            if ( !user ) {
                return;
            }

            $system.$promise.then( ctrl.load );
            unregister();
        });
    };

    ctrl.load = function () {
        if ( !ctrl.user || !$system.user || $system.user.id === ctrl.user.id ) {
            return;
        }

        ctrl.setLoading();

        // Na API following, sucesso significa que segue, enquanto erro
        // significa que não segue.
        return following.one( ctrl.user.username ).get().then(function () {
            ctrl.isFollowing = true;
        }, function () {
            ctrl.isFollowing = false;
        }).finally( ctrl.unsetLoading );
    };

    ctrl.setLoading = function () {
        ctrl.loading = true;
    };

    ctrl.unsetLoading = function () {
        ctrl.loading = false;
    };

    /**
     * Segue o usuário atual
     * @returns     {Promise}
     */
    ctrl.follow = function () {
        ctrl.setLoading();
        return following.post({
            user: ctrl.user.username
        }).finally(function () {
            ctrl.unsetLoading();
            ctrl.isFollowing = true;
        });
    };

    /**
     * Para de seguir o usuário atual
     * @returns     {Promise}
     */
    ctrl.unfollow = function () {
        ctrl.setLoading();
        return following.one( ctrl.user.username ).remove().finally(function () {
            ctrl.unsetLoading();
            ctrl.isFollowing = false;
        });
    };

    // ---------------------------------------------------------------------------------------------

    ctrl.init();
}