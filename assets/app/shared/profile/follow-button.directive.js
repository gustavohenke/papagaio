angular.module( "shared.profile" ).directive( "followButton", followButtonDirective );

function followButtonDirective () {
    var dfn = {};

    dfn.restrict = "E";
    dfn.templateUrl = "app/shared/profile/follow-button.html";
    dfn.controller = "FollowButtonController";
    dfn.controllerAs = "$follow";
    dfn.bindToController = true;
    dfn.scope = {
        user: "="
    };

    return dfn;
}