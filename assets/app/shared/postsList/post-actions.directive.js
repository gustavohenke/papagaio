angular.module( "shared.postsList" ).directive( "postActions", postActionsDirective );

function postActionsDirective () {
    var dfn = {};

    dfn.replace = true;
    dfn.restrict = "E";
    dfn.templateUrl = "app/shared/postsList/post-actions.html";
    dfn.controller = "PostActionsController";
    dfn.controllerAs = "$actions";
    dfn.bindToController = true;
    dfn.scope = {
        post: "=",
        actions: "="
    };

    return dfn;
}