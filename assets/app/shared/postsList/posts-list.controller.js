angular.module( "shared.postsList" ).controller( "PostsListController", PostsListController );

function PostsListController ( postForm ) {
    var ctrl = this;

    /**
     * Abre o formulário de post
     * @param   $event  O evento que originou a abertura do formulário
     */
    ctrl.showForm = function ( $event ) {
        postForm.show( $event );
    };
}