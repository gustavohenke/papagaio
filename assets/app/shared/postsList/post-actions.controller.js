angular.module( "shared.postsList" ).controller( "PostActionsController", PostActionsController );

function PostActionsController ( $scope, likes, maps ) {
    var ctrl = this;
    var unregister = $scope.$watch( "$actions.post.location", function ( location ) {
        if ( !location ) {
            return;
        }

        // Enquanto não temos a localização completa (via geocoding), vamos exibir só as coordenadas
        // por enquanto
        ctrl.location = location.latitude + ", " + location.longitude;

        maps.getAddress( location ).then(function ( loc ) {
            ctrl.location = loc.description;
        });

        unregister();
    });

    /**
     * Determina se uma ação está configurada pra ser usada
     *
     * @param   {string} action
     * @returns {boolean}
     */
    ctrl.has = function ( action ) {
        if ( !ctrl.actions || !_.isArray( ctrl.actions ) ) {
            return false;
        }

        return !!~ctrl.actions.indexOf( action );
    };

    /**
     * Curte ou descurte o post.
     *
     * @returns {Promise}
     */
    ctrl.like = function () {
        var liked = ctrl.post.liked;
        var resource = likes( ctrl.post.id );

        ctrl.post.likesCount += liked ? -1 : 1;
        ctrl.post.liked = !liked;

        return !liked ? resource.put() : resource.remove();
    };

}