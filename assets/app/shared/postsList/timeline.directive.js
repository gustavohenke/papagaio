angular.module( "shared.postsList" ).directive( "timeline", timelineDirective );

function timelineDirective () {
    var dfn = {};

    dfn.replace = true;
    dfn.templateUrl = "app/shared/postsList/timeline.html";
    dfn.controller = "PostsListController";
    dfn.controllerAs = "$posts";
    dfn.bindToController = true;
    dfn.scope = {
        posts: "="
    };

    return dfn;
}