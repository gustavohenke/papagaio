angular.module( "shared.postsList" ).directive( "postContent", postContentDirective );

function postContentDirective () {
    var dfn = {};

    dfn.replace = true;
    dfn.restrict = "E";
    dfn.templateUrl = "app/shared/postsList/post-content.html";
    dfn.scope = {
        post: "="
    };
    dfn.bindToController = false;

    return dfn;
}