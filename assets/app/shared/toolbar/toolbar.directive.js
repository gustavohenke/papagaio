angular.module( "shared.toolbar" ).directive( "toolbar", toolbarDirective );

function toolbarDirective () {
    var dfn = {};

    dfn.controller = "ToolbarController";
    dfn.controllerAs = "$toolbar";
    dfn.templateUrl = "app/shared/toolbar/toolbar.html";
    // dfn.replace = true;
    dfn.restrict = "E";
    dfn.scope = true;

    return dfn;
}