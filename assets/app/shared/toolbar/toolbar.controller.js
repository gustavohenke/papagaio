angular.module( "shared.toolbar" ).controller( "ToolbarController", ToolbarController );

function ToolbarController ( $state, $auth, $system, session, search ) {
    var ctrl = this;

    /**
     * Inicializa o controller
     */
    ctrl.init = function () {
        Object.defineProperty( ctrl, "authenticated", {
            get: function () {
                return $auth.isAuthenticated();
            }
        });
    };

    /**
     * Executa alguma ação do menu.
     *
     * @param   {String} action
     */
    ctrl.call = function ( action ) {
        switch ( action ) {
            case "profile":
                $state.go( "profile", {
                    user: $system.user.username
                });
                break;

            case "config":
                $state.go( "config" );
                break;

            case "logout":
                session.one( $system.session.id ).remove().then(function () {
                    return $auth.logout();
                }).then(function () {
                    $state.go( "home" );
                });
                break;
        }
    };

    /**
     * Realiza pesquisa
     *
     * @param   {String} term
     * @returns {Promise}
     */
    ctrl.search = function ( term ) {
        return search.all( term );
    };

    /**
     * Vai para um estado diferente ao selecionar um resultado de pesquisa.
     * @param   {Object} result
     * @returns {Promise}
     */
    ctrl.selectResult = function ( result ) {
        ctrl.isSearch = false;
        return $state.go( result.state.name, result.state.params );
    };

    // ---------------------------------------------------------------------------------------------

    ctrl.init();
}