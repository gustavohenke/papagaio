# Papagaio

## Tabela de conteúdos
* [Acesso Online](#markdown-header-acesso-online)
* [Estrutura do projeto](#markdown-header-estrutura-do-projeto)
* [Requisitos](#markdown-header-requisitos)

## Acesso Online
O projeto está publicado online via Heroku no endereço https://papagaio.ml.

---

## Estrutura do projeto
* `assets/app`: arquivos da aplicação front-end (HTML, JavaScript e CSS);
* `assets/vendor`: pacotes front-end instalados através do Bower;
* `assets/view`: views renderizadas pelo back-end (HTML);
* `config`: configurações usadas no back-end;
* `public`: o `DOCUMENT_ROOT` da aplicação, ponto de entrada do servidor;
* `src`: namespaces e classes do back-end (PHP);
* `vendor`: pacotes back-end instalados através do Composer. 

---

## Requisitos

* Apache 2
* PHP 5.5
* PostgreSQL 9.2+

### Etapas para instalar o projeto

__No Apache__

1. Habilite `mod_rewrite`;
2. Cria um VirtualHost apontando pra pasta `public` deste projeto:  
   
   ```
   NameVirtualHost *
   <VirtualHost *:2000>
       ServerName papagaioapp.com
       DocumentRoot "<...>\public"
   </VirtualHost>
   ```

__No Postgres__

1. Crie a base de dados:  
   
   ```
   CREATE DATABASE papagaio;
   ```
   
2. Cria a extensão de UUID na base recém criada:  
   
   ```
   CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
   ```
  
__No PHP__
Garanta que a extensão GD está habilitada no `php.ini`. Exemplo:

```
extension=php_gd2.dll
```

Depois, execute os seguintes processos na raíz do projeto:

1. Instale o [Composer](https://getcomposer.org/):  
   
   ```
   php -r "readfile('https://getcomposer.org/installer');" | php
   ```
   
2. Instale os componentes do Composer:  
   
   ```
   php composer.phar install --ignore-platform-reqs
   ```
   
3. Atualize o banco de dados:  
   
   ```
   vendor\bin\doctrine orm:schema-tool:update --force
   ```